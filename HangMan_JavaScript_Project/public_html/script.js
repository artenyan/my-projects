/* Hang Man game project */
/* Created on : Jan 15, 2018, 8:48:23 AM 
    Author : Shushan Artenyan */

//drawing rectangle function
var cleanDraw = function (){   
    var a_canvas = document.getElementById("a");
    var context = a_canvas.getContext("2d"); 
    context.clearRect(0, 0, a_canvas.width, a_canvas.height);   
};

//drawing hanging function
var drawHang = function() {    
    context.beginPath();
    context.moveTo(0,500);
    context.lineTo(150,500);
    context.moveTo(75,500);
    context.lineTo(75,20);
    context.moveTo(75,20);
    context.lineTo(250,20);
    context.moveTo(250,20);
    context.lineTo(250,70);
    context.stroke();  
};


//drawing head function
var drawHead = function(){
    context.beginPath();
    context.arc(250, 100, 30,  2 * Math.PI, false);
    context.stroke();  
};


//drawing neck function
var drawNeck = function(){
    context.beginPath();
    context.moveTo(250,130);
    context.lineTo(250,150);
    context.stroke();

};

//drawing body function
var drawBody = function(){
    context.beginPath();
    context.moveTo(250,150);
    context.lineTo(250,270);
    context.stroke(); 
};


//drawing foot1 function
var drawFoot1 = function(){
    context.beginPath();
    context.moveTo(250,270);
    context.lineTo(190,330);
    context.stroke();
};
//drawing foot2 function
var drawFoot2 = function(){
    context.beginPath();
    context.moveTo(250,270);
    context.lineTo(310,330);
    context.stroke();
};
//drawing hand1 function
var drawHand1 = function(){
    context.beginPath();
    context.moveTo(250,150);
    context.lineTo(220,200);
    context.stroke();
};

//drawing hand2 function
var drawHand2 = function(){
    context.beginPath();
    context.moveTo(250,150);
    context.lineTo(280,200);
    context.stroke();
};

// You lose message function                    
var youLose = function(m){
    var divLose = document.getElementsByClassName("gameOver")[0];
    var aLose = document.createElement("A");
    divLose.appendChild(aLose);
    aLose.innerHTML = "You Lose!!!";
    writingAnswer(m);  
};

//cheque winning function                       
var checkWin = function(){ 
    var count = 0;      
    var line = document.getElementsByClassName("answerLine");       
    for (var k = 0; k < line.length; k++) {
        var line = document.getElementsByClassName("answerLine"); 
        var i = line[k].innerHTML.search("_");
        console.log("index = "+i);
        if (i === -1){
            count++;            
        }        
    }
    if(count === line.length){
        var divWin = document.getElementsByClassName("gameOver")[0];
        var aWin = document.createElement("A");
        divWin.appendChild(aWin);
        aWin.innerHTML = "You Win!!!";             
    }
};

//writting right answer function
var writingAnswer = function(m){    
    switch(m){
        case 1: 
            var arrayList = ["Y","E","R","E","V","A","N"];
            var answer = document.getElementsByClassName("answerLine");
            for (var i = 0; i < arrayList.length; i++) {
                answer[i].innerHTML = arrayList[i];                
            }
            break;
        case 2:
             arrayList = ["A","R","M","E","N","I","A","N"];
            answer = document.getElementsByClassName("answerLine");
            for (var i = 0; i < arrayList.length; i++) {
                answer[i].innerHTML = arrayList[i];    
            }
            break;
        case 3:
            arrayList = ["D","R","A","M"];
            answer = document.getElementsByClassName("answerLine");
            for (var i = 0; i < arrayList.length; i++) {
                answer[i].innerHTML = arrayList[i];    
            }
            break;
    default: break;
    }
};

//starting the game
var a_canvas = document.getElementById("a");
var context = a_canvas.getContext("2d");
drawHang();
var btn = document.getElementsByClassName("btn");

//choosing questions
btn[0].onclick = function(){
    cleanDraw();
    drawHang();
    document.getElementsByClassName("gameOver")[0].innerHTML = "";
    var count = 0; 
    document.getElementById("questionTxt").innerHTML = "The name of capital";
    mackeAnswerLine(7); 
    var ltrBtn = document.getElementsByClassName("letter");
    function addOnClick(n) {
        ltrBtn[n].onclick = function(){
            count = checkAnswer1(1, n, count);
            if (count !== 7){
                checkWin();
            }  
        }; 
    };
    for (var i = 0; i < ltrBtn.length; i++) {
        addOnClick(i);
    }         
};

btn[1].onclick = function(){
    cleanDraw();
    drawHang();
    document.getElementsByClassName("gameOver")[0].innerHTML = "";
    var count = 0; 
    document.getElementById("questionTxt").innerHTML = "The first Christian nation";
    mackeAnswerLine(8);
    var ltrBtn = document.getElementsByClassName("letter");
    function addOnClick(n) {
        ltrBtn[n].onclick = function(){
            count = checkAnswer2(2, n, count);
            if (count !== 7){
                checkWin();
            }  
        }; 
    };
    for (var i = 0; i < ltrBtn.length; i++) {
        addOnClick(i);
    }         
};

btn[2].onclick = function(){ 
    cleanDraw();
    drawHang();
    document.getElementsByClassName("gameOver")[0].innerHTML = "";
    var count = 0; 
    document.getElementById("questionTxt").innerHTML = "Money value name";
    mackeAnswerLine(4);    
    var ltrBtn = document.getElementsByClassName("letter");     
    
//cheque function for all buttons oncklik
function addOnClick(n) {
    ltrBtn[n].onclick = function(){
        count = checkAnswer3(3, n, count);
        if (count !== 7){
            checkWin();
        }            
    }; 
};
for (var i = 0; i < ltrBtn.length; i++) {
    addOnClick(i);
}       
};

//creating lines for each question
var mackeAnswerLine = function (n){
    var div = document.getElementById("answerTxt");
        if (div.innerHTML === ""){
            for (var i = 0; i < n; i++) {
                var ai = document.createElement("A");
                ai.innerHTML = "____";
                div.appendChild(ai);
                ai.className = "answerLine";
            } 
        } else{
            div.innerHTML = "";
            for (var i = 0; i < n; i++) {
                var ai = document.createElement("A");
                ai.innerHTML = "____";
                div.appendChild(ai);
                ai.className = "answerLine";
            }
        }    
};

//chequing Question1
var checkAnswer1 = function(m,n,count){    
    var checked = 0;
    var arrayList = ["Y","E","R","E","V","A","N"];
    var ltrBtn = document.getElementsByClassName("letter")[n];
            for (var j = 0; j < arrayList.length; j++) {                
                var k = arrayList[j].localeCompare(ltrBtn.innerHTML);                              
                if (k === 0){
                    checked++;            
                    document.getElementsByClassName("answerLine")[j].innerHTML = arrayList[j];
                }        
            }    
    if (checked === 0){        
        count++;
        switch(count){
            case 1: drawHead(); break;
            case 2: drawNeck(); break;
            case 3: drawBody(); break;
            case 4: drawFoot1(); break;
            case 5: drawFoot2(); break;
            case 6: drawHand1(); break;
            case 7: drawHand2(); youLose(m); break;            
            default: break;
            }                            
    }
    console.log("count = "+count);
    return count;       
};

//chequing Question2
var checkAnswer2 = function(m,n,count){
    var checked = 0;
    var arrayList = ["A","R","M","E","N","I","A","N"];
    var ltrBtn = document.getElementsByClassName("letter")[n];
            for (var j = 0; j < arrayList.length; j++) {                
                var k = arrayList[j].localeCompare(ltrBtn.innerHTML);                                            
                if (k === 0){
                    checked++;            
                    document.getElementsByClassName("answerLine")[j].innerHTML = arrayList[j];
                }        
            }    
    if (checked === 0){        
        count++;
        switch(count){
            case 1: drawHead(); break;
            case 2: drawNeck(); break;
            case 3: drawBody(); break;
            case 4: drawFoot1(); break;
            case 5: drawFoot2(); break;
            case 6: drawHand1(); break;
            case 7: drawHand2(); youLose(m); break;            
            default: break;
            }                            
    }
    console.log("count = "+count);
    return count;       
};

//chequing Question3
var checkAnswer3 = function(m,n,count){      
    var checked = 0;
    var arrayList = ["D","R","A","M"];
    var ltrBtn = document.getElementsByClassName("letter")[n];
            for (var j = 0; j < arrayList.length; j++) {                
                var k = arrayList[j].localeCompare(ltrBtn.innerHTML);                                              
                if (k === 0){
                    checked++;            
                    document.getElementsByClassName("answerLine")[j].innerHTML = arrayList[j];
                }        
            }    
    if (checked === 0){        
        count++;
        switch(count){
            case 1: drawHead(); break;
            case 2: drawNeck(); break;
            case 3: drawBody(); break;
            case 4: drawFoot1(); break;
            case 5: drawFoot2(); break;
            case 6: drawHand1(); break;
            case 7: drawHand2(); youLose(m); break;            
            default: break;
            }                            
    }
    return count;       
};