﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for ModifyCustomer.xaml
    /// </summary>
    public partial class ModifyCustomer : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        int idCust;
        public ModifyCustomer(int Id, string firstName, string lastName, string address, string city, string postalCode, string country, string phone, string email)
        {
            InitializeComponent();
            idCust = Id;
            txtFirstName.Text = firstName;
            txtLastName.Text = lastName;
            txtAddress.Text = address;
            txtCity.Text = city;
            txtPostalCode.Text = postalCode;
            txtCountry.Text = country;
            txtPhone.Text = phone;
            txtEmail.Text = email;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            startclock();
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void btnUpdateClient_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (txtLastName.Text == "" || txtFirstName.Text == "")
            {
                MessageBoxResult result = MessageBox.Show("First name and Last name are required");
                //string required = "First name is required";
                //lblFNameRequired.Content = required;
            }
            //First Name and Last Name must be contain only letters
            else if (!Regex.IsMatch(txtFirstName.Text, @"^[\p{L}]+$")
                    || !Regex.IsMatch(txtLastName.Text, @"^[\p{L}]+$"))
            {
                MessageBox.Show("First Name and Last Name must contain only letters");
                //string required = "First name must contain only letters";
                //lblFNameRequired.Content = required;
            }
            // Email address must be valid one
            else if (txtEmail.Text != "" & !regex.IsMatch(txtEmail.Text))
            {
                MessageBox.Show("Please enter a valid email address");
            }
            //Phone number must be in required format
            else if (txtPhone.Text != "" & !Regex.IsMatch(txtPhone.Text, "\\p{N}{3}-\\p{N}{3}-\\p{N}{4}\\b"))
            {
                MessageBox.Show("Phone number format is:\n 555-555-5555");
            }
            else
            {

                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;

                string address = txtAddress.Text;
                string city = txtCity.Text;
                string postalCode = txtPostalCode.Text;
                string country = txtCountry.Text;
                string phone = txtPhone.Text;
                string email = txtEmail.Text;
                int i = objWebservice.editCustomer(idCust, firstName, lastName, address, city, postalCode, country, phone, email);
                if (i >= 1)
                {
                    MessageBox.Show("Customer updated successfully");
                }
                else
                {
                    MessageBox.Show("Error update");
                }
                FindCustomer nc = new FindCustomer();
                nc.Show();
                this.Hide();
            }
        }
        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }
        private void newReceipt_Click(object sender, RoutedEventArgs e)
        {
            CreateReceipt cr = new CreateReceipt();
            cr.Show();
            this.Hide();
        }

        private void findReceipt_Click(object sender, RoutedEventArgs e)
        {
            ReceiptsList rl = new ReceiptsList();
            rl.Show();
            this.Hide();
        }

        private void printReport_Click(object sender, RoutedEventArgs e)
        {
            PrintReport pr = new PrintReport();
            pr.Show();
            this.Hide();
        }
    }
}
