﻿using HiDentClient.localhost;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for ModifyReceipt.xaml
    /// </summary>
    public partial class ModifyReceipt : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        public ModifyReceipt(int Id, String firstname, 
            String lastName, String serviceName, String price, 
            String MedicalIdNumber, DateTime date)
        {
            InitializeComponent();
            startclock();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            
            txtReceiptNumber.Text = Id.ToString();
            //Doctor comboBox
            List<ComboData> doctorData = new List<ComboData>(objWebservice.getDoctorsList());
            cbDoctorNumber.ItemsSource = doctorData;
            cbDoctorNumber.DisplayMemberPath = "Value";
            cbDoctorNumber.SelectedValuePath = "Id";
            cbDoctorNumber.Text = MedicalIdNumber;

            txtCustFirstName.Text = firstname;
            txtCustLastName.Text = lastName;

            //services comboBox
            List<ComboData> comboData = new List<ComboData>(objWebservice.getAllServices());
            comboData.AddRange(objWebservice.getAllServices());
            cbServices.ItemsSource = comboData;
            cbServices.DisplayMemberPath = "Value";
            cbServices.SelectedValuePath = "Id";
            cbServices.Text = serviceName;

            txtAmount.Text = price;               
            dtpDate.SelectedDate = date; 

        }

       

        private void btnSaveChanges_Click(object sender, RoutedEventArgs e)
        {
            int receiptNumber = int.Parse(txtReceiptNumber.Text);
            string firstName = txtCustFirstName.Text;
            string lastName = txtCustLastName.Text;
            //check for customer name
            if(firstName == "" || lastName == "")
            {
                MessageBox.Show("First name and Lact name are required ");
            }
            else if (objWebservice.checkCustomer(firstName, lastName) == "")
            {
                MessageBox.Show("You don't have a customer under name of " + firstName + " " + lastName);
            }
            else 
            {                
                int custID = int.Parse(objWebservice.checkCustomer(firstName, lastName));
                //check for Doctor Number comboBox
                if (cbDoctorNumber.SelectedIndex == 0)
                {
                    MessageBox.Show("Please choose a doctor");
                }
                else
                {
                    int doctorID = int.Parse(cbDoctorNumber.SelectedValue.ToString());
                    //check for Service comboBox
                    if (cbServices.SelectedIndex == 0)
                    {
                        MessageBox.Show("Please choose a service");
                    }
                    else
                    {
                        int serviceID = int.Parse(cbServices.SelectedValue.ToString());
                        //check for date
                        if (dtpDate.SelectedDate == null)
                        {
                            MessageBox.Show("Please choose a date");
                        }
                        else
                        {
                            DateTime date = DateTime.Parse(dtpDate.SelectedDate.ToString());
                            if (objWebservice.updateReceipt(receiptNumber,doctorID, custID, serviceID, date) > 0)
                            {
                                MessageBox.Show("Your receipt was modified and saved successfully");
                                new ReceiptsList().Show();
                                this.Hide();
                            }
                            else
                            {
                                MessageBox.Show("Your receipt was not modified there are some issues!!!");
                            }
                        }
                    }
                }
            }           
        }

        private void cbServices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int ServiceID = int.Parse(cbServices.SelectedValue.ToString());
            txtAmount.Text = objWebservice.getServicePriceById(ServiceID);
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            cbDoctorNumber.SelectedIndex = 0;
            txtCustFirstName.Text = "";
            txtCustLastName.Text = "";
            cbServices.SelectedIndex = 0;
            txtAmount.Text = "";
            dtpDate.Text = null;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            new ReceiptsList().Show();
            this.Hide();
        }
        //Menu Bar
        #region
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void miNewDoctor_Click(object sender, RoutedEventArgs e)
        {
            NewDoctor nd = new NewDoctor();
            nd.Show();
            this.Hide();
        }

        private void miDoctorsList_Click(object sender, RoutedEventArgs e)
        {
            DoctorsList dl = new DoctorsList();
            dl.Show();
            this.Hide();
        }

        private void newReceipt_Click(object sender, RoutedEventArgs e)
        {
            CreateReceipt cr = new CreateReceipt();
            cr.Show();
            this.Hide();
        }

        private void findReceipt_Click(object sender, RoutedEventArgs e)
        {
            ReceiptsList rl = new ReceiptsList();
            rl.Show();
            this.Hide();
        }

        private void printReport_Click(object sender, RoutedEventArgs e)
        {
            PrintReport pr = new PrintReport();
            pr.Show();
            this.Hide();
        }


        private void miChart_Click(object sender, RoutedEventArgs e)
        {
            chart ch = new chart();
            ch.Show();
            this.Hide();
        }
        #endregion
    }
}
