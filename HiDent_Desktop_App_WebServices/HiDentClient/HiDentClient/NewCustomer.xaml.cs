﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for NewCustomer.xaml
    /// </summary>
    public partial class NewCustomer : Window
    {
        //creating the object of web service class  
        localhost.WebService objWebservice = new localhost.WebService();

        string firstName, lastName, address, city, postalCode, country, phone, email;

        public NewCustomer()
        {
            startclock();
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void btnAddClient_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            //checking if required fields are empty or not
            if (txtLastName.Text == "" || txtFirstName.Text == "")
            {
                MessageBoxResult result = MessageBox.Show("First name and Last name are required");
                //string required = "First name is required";
                //lblFNameRequired.Content = required;
            }
            //First Name and Last Name must be contain only letters
            else if (!Regex.IsMatch(txtFirstName.Text, @"^[\p{L}]+$")
                    || !Regex.IsMatch(txtLastName.Text, @"^[\p{L}]+$"))
            {
                MessageBox.Show("First Name and Last Name must contain only letters");
                //string required = "First name must contain only letters";
                //lblFNameRequired.Content = required;
            }
            // Email address must be valid one
            else if (txtEmail.Text != "" & !regex.IsMatch(txtEmail.Text))
            {
                MessageBox.Show("Please enter a valid email address");
            }
            //Phone number must be in required format
            else if (txtPhone.Text != "" & !Regex.IsMatch(txtPhone.Text, "\\p{N}{3}-\\p{N}{3}-\\p{N}{4}\\b"))
            {
                MessageBox.Show("Phone number format is:\n 555-555-5555");
            } else
            {
                firstName = txtFirstName.Text;
                lastName = txtLastName.Text;
                address = txtAddress.Text;
                city = txtCity.Text;
                postalCode = txtPostalCode.Text;
                country = txtCountry.Text;
                phone = txtPhone.Text;
                email = txtEmail.Text;

                // Check this customer for his existance
                string j = objWebservice.checkCustomer(firstName, lastName);

                if (j == "")
                {
                    // Insert this new customer
                    int i = objWebservice.AddNewCustomer(firstName, lastName, address, city, postalCode, country, phone, email);

                    if (i != 0)
                    {
                        if (MessageBox.Show("Customer information was saved successfully.\n" +
                            "Do you want to add another Customer?",
                            "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            //this.Show();
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            txtAddress.Text = "";
                            txtCity.Text = "";
                            txtPostalCode.Text = "";
                            txtCountry.Text = "";
                            txtPhone.Text = "";
                            txtEmail.Text = "";
                        }
                        else
                        {
                            this.Hide();
                            new FindCustomer().Show();
                        }
                    }
                    
                }
                else //Customer already in DB
                {
                    MessageBox.Show("This client is already registered");
                }
            }
        }
        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }
        private void newReceipt_Click(object sender, RoutedEventArgs e)
        {
            CreateReceipt cr = new CreateReceipt();
            cr.Show();
            this.Hide();
        }

        private void findReceipt_Click(object sender, RoutedEventArgs e)
        {
            ReceiptsList rl = new ReceiptsList();
            rl.Show();
            this.Hide();
        }

        private void printReport_Click(object sender, RoutedEventArgs e)
        {
            PrintReport pr = new PrintReport();
            pr.Show();
            this.Hide();
        }
        private void miNewDoctor_Click(object sender, RoutedEventArgs e)
        {
            NewDoctor nd = new NewDoctor();
            nd.Show();
            this.Hide();
        }

        private void miDoctorsList_Click(object sender, RoutedEventArgs e)
        {
            DoctorsList dl = new DoctorsList();
            dl.Show();
            this.Hide();
        }

        private void miChart_Click(object sender, RoutedEventArgs e)
        {
            chart ch = new chart();
            ch.Show();
            this.Hide();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtPostalCode.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
        }

    }//end partial class
}//end namespace
    

