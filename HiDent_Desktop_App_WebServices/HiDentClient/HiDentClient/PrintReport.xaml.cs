﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.IO;

using HiDentClient.localhost;
using System.IO.Packaging;
using System.Windows.Threading;
using System.Windows.Forms;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Diagnostics;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for PrintReport.xaml
    /// </summary>
    public partial class PrintReport : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        public PrintReport()
        {
            InitializeComponent();
            startclock();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            //Doctor comboBox
            List<ComboData> doctorData = new List<ComboData>(objWebservice.getDoctorsList());
            cbDoctorNumber.ItemsSource = doctorData;
            cbDoctorNumber.DisplayMemberPath = "Value";
            cbDoctorNumber.SelectedValuePath = "Id";
            cbDoctorNumber.SelectedIndex = 0;

            //Service comboBox
            List<ComboData> serviceData = new List<ComboData>(objWebservice.getAllServices());
            cbServices.ItemsSource = serviceData;
            cbServices.DisplayMemberPath = "Value";
            cbServices.SelectedValuePath = "Id";
            cbServices.SelectedIndex = 0;

            rbYear.IsChecked = true;
        }

        public void saveIntoFile(string[] reportList)
        {            
            PdfDocument document = new PdfDocument();
            PdfPage page = document.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XFont font = new XFont("Verdana", 8, XFontStyle.Bold);
            XImage image = XImage.FromFile("C:/Documents C/ws-project-2/HiDentClient/HiDentClient/Images/printLogo.jpg");
            int x = 7; int y = 150; int count = 0; int n = 0;
            foreach(string report in reportList)
            {               
                gfx.DrawString(report, font, XBrushes.Black, new XRect(x, y, page.Width, page.Height), XStringFormat.TopLeft);
                x += 45 + n;
                n += 10;
                count++;                
                if(count == 5){n += 80;}
                if(count >= 7)
                {
                    font = new XFont("Verdana", 8, XFontStyle.Regular); y += 10; count = 0; x = 7; n = 0;
                }
            }
            gfx.DrawImage(image, new XRect(30, 30, 80, 77));
            gfx.DrawString("HI DENT CLINIC", new XFont("Aharoni", 40, XFontStyle.Bold), XBrushes.DarkBlue, new XRect(30, 40, page.Width, page.Height), XStringFormat.TopCenter);
            string filename = "HelloWorld.pdf";
            document.Save(filename);
            Process.Start(filename);
        }

        public void checkRadioButtons()
        {
            string docNumId = cbDoctorNumber.Text;
            string serviceName = cbServices.Text;            
            string firstName = txtFirstName.Text;
            string lastName = txtLastName.Text;
            string[] reportList = null;
            if (rbYear.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByYear(int.Parse(cbYear.Text.ToString()));
                saveIntoFile(reportList);
            }
            else if (rbYearAndMonth.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByYearAndMonth(int.Parse(cbYear.Text.ToString()), int.Parse(cbMonth.Text.ToString()));
                saveIntoFile(reportList);
            }
            else if (rbYearAndCustName.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByYearAndCustName(int.Parse(cbYear.Text.ToString()), firstName, lastName);
                saveIntoFile(reportList);
            }
            else if (rbYearMonthName.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByYearMonthAndName(int.Parse(cbYear.Text.ToString()), int.Parse(cbMonth.Text.ToString()), firstName, lastName);
                saveIntoFile(reportList);
            }
            else if (rbSpecifyDate.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByDate(DateTime.Parse(dpDate.Text.ToString()));
                saveIntoFile(reportList);
            }
            else if (rbDateName.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByDateAndName(DateTime.Parse(dpDate.Text.ToString()), firstName, lastName);
                saveIntoFile(reportList);
            }
            else if (rbByCustName.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByCustName(firstName, lastName);
                saveIntoFile(reportList);
            }
            else if (rbDoctorIdNumber.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByDoctorNumId(docNumId);
                saveIntoFile(reportList);
            }
            else if (rbServiceName.IsChecked == true)
            {
                reportList = objWebservice.getReceiptByServiceName(serviceName);
                saveIntoFile(reportList);
            }
        }

        private void btPrint_Click(object sender, RoutedEventArgs e)
        {             
            if ((rbYear.IsChecked == true & cbYear.SelectedIndex == 0)
                || (rbYearAndMonth.IsChecked == true & (cbYear.SelectedIndex == 0 || cbMonth.SelectedIndex == 0))
                || (rbSpecifyDate.IsChecked == true & dpDate.SelectedDate == null )
                || (rbByCustName.IsChecked == true & (txtLastName.Text == "" || txtFirstName.Text == ""))
                || (rbYearAndCustName.IsChecked == true & (cbYear.SelectedIndex == 0 || txtLastName.Text == "" || txtFirstName.Text == ""))                 
                || (rbYearMonthName.IsChecked == true & (cbYear.SelectedIndex == 0 || cbMonth.SelectedIndex == 0 || txtLastName.Text == "" || txtFirstName.Text == ""))
                || (rbDateName.IsChecked == true & (dpDate.SelectedDate == null || txtLastName.Text == "" || txtFirstName.Text == ""))
                || (rbServiceName.IsChecked == true & cbServices.SelectedIndex == 0)
                || (rbDoctorIdNumber.IsChecked == true & cbDoctorNumber.SelectedIndex == 0))

            {
                System.Windows.Forms.MessageBox.Show("Please fill all active text boxes before Print Preview");
            }
            //First Name and Last Name must be contain only letters
            else if (txtFirstName.IsEnabled == true & !Regex.IsMatch(txtFirstName.Text, @"^[\p{L}]+$"))
            {
                System.Windows.Forms.MessageBox.Show("First Name must be contain only letters", "Error Message");
            }
            else if (txtLastName.IsEnabled == true & !Regex.IsMatch(txtLastName.Text, @"^[\p{L}]+$"))
            {
                System.Windows.Forms.MessageBox.Show("Last Name must be contain only letters", "Error Message");
            }
            else if (txtLastName.Text != "" & txtFirstName.Text != "")
            {
                //Customer check in database                
                String FirstName = txtFirstName.Text.ToString();
                String LastName = txtLastName.Text.ToString();
                string Id = objWebservice.checkCustomer(FirstName, LastName);

                if (Id != "")
                {
                    checkRadioButtons();
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("You don't have a customer in this name!!!");
                }

            }
            else
            {
                checkRadioButtons();
            }
            

        }

        //Radio buttons events
        #region
          private void rbYear_Checked(object sender, RoutedEventArgs e)
           {
               cbDoctorNumber.SelectedIndex = 0;
               cbServices.SelectedIndex = 0;
               cbMonth.SelectedIndex = 0;
               dpDate.Text = null;
               txtFirstName.Text = "";
               txtLastName.Text = "";

               cbDoctorNumber.IsEnabled = false;
               cbServices.IsEnabled = false;
               cbYear.IsEnabled = true;
               cbMonth.IsEnabled = false;
               dpDate.IsEnabled = false;
               txtFirstName.IsEnabled = false;
               txtLastName.IsEnabled = false;
           }

           private void rbYearAndMonth_Checked(object sender, RoutedEventArgs e)
           {
                cbDoctorNumber.SelectedIndex = 0;
                cbServices.SelectedIndex = 0;            
                txtFirstName.Text = "";
                txtLastName.Text = "";
                dpDate.Text = null;

                cbDoctorNumber.IsEnabled = false;
                cbServices.IsEnabled = false;
                cbYear.IsEnabled = true;
                cbMonth.IsEnabled = true;
                dpDate.IsEnabled = false;
                txtFirstName.IsEnabled = false;
                txtLastName.IsEnabled = false;
           }

           private void rbSpecifyDate_Checked(object sender, RoutedEventArgs e)
           {
                cbDoctorNumber.SelectedIndex = 0;
                cbServices.SelectedIndex = 0;
                cbMonth.SelectedIndex = 0;
                cbYear.SelectedIndex = 0;
                txtFirstName.Text = "";
                txtLastName.Text = "";               

                cbDoctorNumber.IsEnabled = false;
                cbServices.IsEnabled = false;
                cbYear.IsEnabled = false;
                cbMonth.IsEnabled = false;
                dpDate.IsEnabled = true;
                txtFirstName.IsEnabled = false;
                txtLastName.IsEnabled = false;
           }

           private void rbByCustName_Checked(object sender, RoutedEventArgs e)
           {
            //Clean not needed fields
                cbDoctorNumber.SelectedIndex = 0;
                cbServices.SelectedIndex = 0;
                cbMonth.SelectedIndex = 0;
                cbYear.SelectedIndex = 0;
                dpDate.Text = null;

                //Made needed fields active
                cbDoctorNumber.IsEnabled = false;
                cbServices.IsEnabled = false;
                cbYear.IsEnabled = false;
                cbMonth.IsEnabled = false;
                dpDate.IsEnabled = false;
                txtFirstName.IsEnabled = true;
                txtLastName.IsEnabled = true;            
           }

           private void rbYearAndCustName_Checked(object sender, RoutedEventArgs e)
           {
                cbDoctorNumber.SelectedIndex = 0;
                cbServices.SelectedIndex = 0;
                cbMonth.SelectedIndex = 0;           
                dpDate.Text = null;

                cbDoctorNumber.IsEnabled = false;
                cbServices.IsEnabled = false;
                cbYear.IsEnabled = true;
                cbMonth.IsEnabled = false;
                dpDate.IsEnabled = false;
                txtFirstName.IsEnabled = true;
                txtLastName.IsEnabled = true;
           }

           private void rbYearMonthName_Checked(object sender, RoutedEventArgs e)
           {
                cbDoctorNumber.SelectedIndex = 0;
                cbServices.SelectedIndex = 0;            
                dpDate.Text = null;

                cbDoctorNumber.IsEnabled = false;
                cbServices.IsEnabled = false;
                cbYear.IsEnabled = true;
                cbMonth.IsEnabled = true;
                dpDate.IsEnabled = false;
                txtFirstName.IsEnabled = true;
                txtLastName.IsEnabled = true;
           }

           private void rbDateName_Checked(object sender, RoutedEventArgs e)
           {
                cbDoctorNumber.SelectedIndex = 0;
                cbServices.SelectedIndex = 0;
                cbMonth.SelectedIndex = 0;
                cbYear.SelectedIndex = 0;

                cbDoctorNumber.IsEnabled = false;
                cbServices.IsEnabled = false;
                cbYear.IsEnabled = false;
                cbMonth.IsEnabled = false;
                dpDate.IsEnabled = true;
                txtFirstName.IsEnabled = true;
                txtLastName.IsEnabled = true;
           }

           private void rbServiceName_Checked(object sender, RoutedEventArgs e)
           {
                cbDoctorNumber.SelectedIndex = 0;            
                cbMonth.SelectedIndex = 0;
                cbYear.SelectedIndex = 0;
                txtFirstName.Text = "";
                txtLastName.Text = "";
                dpDate.Text = null;

               cbDoctorNumber.IsEnabled = false;
               cbServices.IsEnabled = true;
               cbYear.IsEnabled = false;
               cbMonth.IsEnabled = false;
               dpDate.IsEnabled = false;
               txtFirstName.IsEnabled = false;
               txtLastName.IsEnabled = false;
           }

           private void rbDoctorIdNumber_Checked(object sender, RoutedEventArgs e)
           {
                txtFirstName.Text = "";
                txtLastName.Text = "";            
                cbServices.SelectedIndex = 0;
                cbMonth.SelectedIndex = 0;
                cbYear.SelectedIndex = 0;
                dpDate.Text = null;

                cbDoctorNumber.IsEnabled = true;
                cbServices.IsEnabled = false;
                cbYear.IsEnabled = false;
                cbMonth.IsEnabled = false;
                dpDate.IsEnabled = false;
                txtFirstName.IsEnabled = false;
                txtLastName.IsEnabled = false;
           }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var message = "Are you finish your job with creating report?";
            string caption = "Message";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.
            result = System.Windows.Forms.MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                new MainWindow().Show();
                this.Close();
            }

        }

        //Menu Bar
        #region
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void miNewDoctor_Click(object sender, RoutedEventArgs e)
        {
            NewDoctor nd = new NewDoctor();
            nd.Show();
            this.Hide();
        }

        private void miDoctorsList_Click(object sender, RoutedEventArgs e)
        {
            DoctorsList dl = new DoctorsList();
            dl.Show();
            this.Hide();
        }

        private void newReceipt_Click(object sender, RoutedEventArgs e)
        {
            CreateReceipt cr = new CreateReceipt();
            cr.Show();
            this.Hide();
        }

        private void findReceipt_Click(object sender, RoutedEventArgs e)
        {
            ReceiptsList rl = new ReceiptsList();
            rl.Show();
            this.Hide();
        }

        private void printReport_Click(object sender, RoutedEventArgs e)
        {
            PrintReport pr = new PrintReport();
            pr.Show();
            this.Hide();
        }


        private void miChart_Click(object sender, RoutedEventArgs e)
        {
            chart ch = new chart();
            ch.Show();
            this.Hide();
        }
        #endregion
    }
}