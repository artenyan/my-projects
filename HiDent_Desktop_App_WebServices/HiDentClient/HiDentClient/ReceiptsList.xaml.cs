﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for ReceiptsList.xaml
    /// </summary>
  
        public partial class ReceiptsList : Window
        {
            localhost.WebService objWebservice = new localhost.WebService();
            public ReceiptsList()
            {
                InitializeComponent();
                ViewReceiptsDataGridView.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getAllReceiptsAsStrings() });
                startclock();
                WindowStartupLocation = WindowStartupLocation.CenterScreen;
                int count = ViewReceiptsDataGridView.Items.Count;
                lblCount.Content = "Total receipts: " + (count - 1);
            

            }

            private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
            {
                string firstName = txtSearch.Text;
                ViewReceiptsDataGridView.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getReceiptByCustomerFirsName(firstName) });
            }

            private void btnDelete_Click(object sender, RoutedEventArgs e)
            {
                if (ViewReceiptsDataGridView.SelectedItems.Count <= 0)
                {
                    MessageBox.Show("For Delete you need to select one of the receipts");
                }
                else
                {
                    DataRowView rowView = ViewReceiptsDataGridView.SelectedItem as DataRowView;
                    string receiptID = rowView.Row[0].ToString();
                    int id = int.Parse(receiptID);
                    if (MessageBox.Show("You Realy want to delete number " + receiptID + " Receipt information?", "Message", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        if (objWebservice.deleteReceipt(id) == 1)
                        {
                            MessageBox.Show("Receipt deleted successfully");
                            ViewReceiptsDataGridView.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getAllReceiptsAsStrings() });
                        }
                        else
                        {
                            MessageBox.Show("You could not delete.There are some issue.");
                        }

                        int count = ViewReceiptsDataGridView.Items.Count;
                        lblCount.Content = "Total clients: " + (count - 1);
                    }
                }
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            if (ViewReceiptsDataGridView.SelectedItems.Count <= 0)
            {
                MessageBox.Show("For Modify you need to select one of the receipts");
            }
            else
            {
                DataRowView rowView = ViewReceiptsDataGridView.SelectedItem as DataRowView;
                string receiptID = rowView.Row[0].ToString();
                int Id = int.Parse(receiptID);
                string firstname = rowView.Row[1].ToString();
                string lastName = rowView.Row[2].ToString();
                string serviceName = rowView.Row[3].ToString();
                string price = rowView.Row[4].ToString();
                string MedicalIdNumber = rowView.Row[5].ToString();
                DateTime date = DateTime.Parse(rowView.Row[6].ToString());
                ModifyReceipt mr = new ModifyReceipt(
                    Id, firstname, lastName, serviceName, price, MedicalIdNumber, date);
                mr.Show();  // or mr.Showdialog();
                this.Hide();
            }
        }



        //Menu Bar
        #region
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void miNewDoctor_Click(object sender, RoutedEventArgs e)
        {
            NewDoctor nd = new NewDoctor();
            nd.Show();
            this.Hide();
        }

        private void miDoctorsList_Click(object sender, RoutedEventArgs e)
        {
            DoctorsList dl = new DoctorsList();
            dl.Show();
            this.Hide();
        }

        private void newReceipt_Click(object sender, RoutedEventArgs e)
        {
            CreateReceipt cr = new CreateReceipt();
            cr.Show();
            this.Hide();
        }

        private void findReceipt_Click(object sender, RoutedEventArgs e)
        {
            ReceiptsList rl = new ReceiptsList();
            rl.Show();
            this.Hide();
        }

        private void printReport_Click(object sender, RoutedEventArgs e)
        {
            PrintReport pr = new PrintReport();
            pr.Show();
            this.Hide();
        }


        private void miChart_Click(object sender, RoutedEventArgs e)
        {
            chart ch = new chart();
            ch.Show();
            this.Hide();
        }
        #endregion
    }
}
