﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for ServicesList.xaml
    /// </summary>
    public partial class ServicesList : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        int count;
        public ServicesList()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            startclock();
            dgServices.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getServicesList() });
            count = dgServices.Items.Count;
            lblCount.Content = "Total services: " + (count-1);
        }

        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }
        private void miNewDoctor_Click(object sender, RoutedEventArgs e)
        {
            NewDoctor nd = new NewDoctor();
            nd.Show();
            this.Hide();
        }

        private void miDoctorsList_Click(object sender, RoutedEventArgs e)
        {
            DoctorsList dl = new DoctorsList();
            dl.Show();
            this.Hide();
        }
        private void newReceipt_Click(object sender, RoutedEventArgs e)
        {
            CreateReceipt cr = new CreateReceipt();
            cr.Show();
            this.Hide();
        }

        private void findReceipt_Click(object sender, RoutedEventArgs e)
        {
            ReceiptsList rl = new ReceiptsList();
            rl.Show();
            this.Hide();
        }
        private void miChart_Click(object sender, RoutedEventArgs e)
        {
            chart ch = new chart();
            ch.Show();
            this.Hide();
        }

        private void printReport_Click(object sender, RoutedEventArgs e)
        {
            PrintReport pr = new PrintReport();
            pr.Show();
            this.Hide();
        }

        private void txtSearchService_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgServices.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.searchService(txtSearchService.Text) });
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            if (dgServices.SelectedItems.Count <= 0)
            {
                MessageBox.Show("For Modify you need to choose a service");
            }
            else
            {
                DataRowView rowview = dgServices.SelectedItem as DataRowView;
                int id = int.Parse(rowview.Row[0].ToString());
                string serviceName = rowview.Row[1].ToString();
                double Price = double.Parse(rowview.Row[2].ToString());

                ModifyService nc = new ModifyService(id, serviceName, Price);
                nc.Show();
                this.Hide();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dgServices.SelectedItems.Count <= 0)
            {
                MessageBox.Show("For Delete you need to choose a Service");
            }
            else
            {
                DataRowView rowview = dgServices.SelectedItem as DataRowView;
                int id = int.Parse(rowview.Row[0].ToString());

                if (MessageBox.Show("Are you Sure you want to Delete " + rowview.Row[1].ToString() + "?",
                                             "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int j = objWebservice.deleteService(id);
                    if (j > 0)
                    {
                        MessageBox.Show("Record Deleted Successfully!");
                        dgServices.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getServicesList() });
                    }
                    this.Show();
                }
                count = dgServices.Items.Count;
                lblCount.Content = "Total services: " + (count - 1);
            }
        }
    }
}
