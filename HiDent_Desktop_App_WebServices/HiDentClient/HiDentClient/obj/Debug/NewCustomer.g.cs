﻿#pragma checksum "..\..\NewCustomer.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8D123073E9E7B36220EC535E86B9E3AA6DE17D5B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using HiDentClient;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace HiDentClient {
    
    
    /// <summary>
    /// NewCustomer
    /// </summary>
    public partial class NewCustomer : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem miNewClient;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem miFind_Client;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem miExit;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem newReceipt;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem findReceipt;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem printReport;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCurrentTime;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFirstName;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtLastName;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtAddress;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCity;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPostalCode;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCountry;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPhone;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEmail;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAddClient;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnReset;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancel;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblFNameRequired;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblLNameRequired;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\NewCustomer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCustID;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/HiDentClient;component/newcustomer.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\NewCustomer.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.miNewClient = ((System.Windows.Controls.MenuItem)(target));
            
            #line 14 "..\..\NewCustomer.xaml"
            this.miNewClient.Click += new System.Windows.RoutedEventHandler(this.miNewClient_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.miFind_Client = ((System.Windows.Controls.MenuItem)(target));
            
            #line 15 "..\..\NewCustomer.xaml"
            this.miFind_Client.Click += new System.Windows.RoutedEventHandler(this.miFind_Client_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.miExit = ((System.Windows.Controls.MenuItem)(target));
            
            #line 17 "..\..\NewCustomer.xaml"
            this.miExit.Click += new System.Windows.RoutedEventHandler(this.miExit_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.newReceipt = ((System.Windows.Controls.MenuItem)(target));
            
            #line 20 "..\..\NewCustomer.xaml"
            this.newReceipt.Click += new System.Windows.RoutedEventHandler(this.newReceipt_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.findReceipt = ((System.Windows.Controls.MenuItem)(target));
            
            #line 21 "..\..\NewCustomer.xaml"
            this.findReceipt.Click += new System.Windows.RoutedEventHandler(this.findReceipt_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 24 "..\..\NewCustomer.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.miNewService_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 25 "..\..\NewCustomer.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.miServiceList_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 28 "..\..\NewCustomer.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.miNewDoctor_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 29 "..\..\NewCustomer.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.miDoctorsList_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.printReport = ((System.Windows.Controls.MenuItem)(target));
            
            #line 32 "..\..\NewCustomer.xaml"
            this.printReport.Click += new System.Windows.RoutedEventHandler(this.printReport_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 35 "..\..\NewCustomer.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.miChart_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.lblCurrentTime = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.txtFirstName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.txtLastName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.txtAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.txtCity = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.txtPostalCode = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.txtCountry = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.txtPhone = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.txtEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.btnAddClient = ((System.Windows.Controls.Button)(target));
            
            #line 72 "..\..\NewCustomer.xaml"
            this.btnAddClient.Click += new System.Windows.RoutedEventHandler(this.btnAddClient_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.btnReset = ((System.Windows.Controls.Button)(target));
            
            #line 73 "..\..\NewCustomer.xaml"
            this.btnReset.Click += new System.Windows.RoutedEventHandler(this.btnReset_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.btnCancel = ((System.Windows.Controls.Button)(target));
            
            #line 74 "..\..\NewCustomer.xaml"
            this.btnCancel.Click += new System.Windows.RoutedEventHandler(this.btnCancel_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.lblFNameRequired = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.lblLNameRequired = ((System.Windows.Controls.Label)(target));
            return;
            case 26:
            this.lblCustID = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

