﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace HiDentServer
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        SqlCommand cmd;
        SqlConnection con;
       
        public class ComboData
        {
            public int Id { get; set; }
            public string Value { get; set; }
        }

        /*Nailia - Customer*/
        [WebMethod]
        public string checkCustomer(string firstName, string lastName)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string j = "";

            //check if the new customer already exist
            string qryCheck = "select CustID from Customer where CustFirstName = '" + firstName + "' and CustLastName = '" + lastName + "'";
            using (SqlCommand cmd = new SqlCommand(qryCheck, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var custID = reader["CustID"];
                    j = custID.ToString();
                }
            }
            return j;
        }

        [WebMethod]
        public int AddNewCustomer(string firstName, string lastName, string address, string city, string postalCode, string country, string phone, string email)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            cmd = new SqlCommand("INSERT INTO Customer (CustFirstName, CustLastName, Address, City, ZipCode, Country, Phone, EmailAddress) " +
            "VALUES (@firstName, @lastName, @address, @city, @postalCode, @country, @phone, @email)", con);
            cmd.Parameters.AddWithValue("@firstName", firstName);
            cmd.Parameters.AddWithValue("@lastName", lastName);
            cmd.Parameters.AddWithValue("@address", address);
            cmd.Parameters.AddWithValue("@city", city);
            cmd.Parameters.AddWithValue("@postalCode", postalCode);
            cmd.Parameters.AddWithValue("@country", country);
            cmd.Parameters.AddWithValue("@phone", phone);
            cmd.Parameters.AddWithValue("@email", email);
            int i = cmd.ExecuteNonQuery();
            con.Close();

            return i;
        }

        [WebMethod]
        public DataTable getCustomersList()
        {
            DataTable dtCustomers = new DataTable();
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string query = "SELECT * FROM Customer";
            //string query = "SELECT CustID as 'id', CustFirstName as 'First name', CustLastName as 'Last name', Address as 'Address'," +
            //    "City, ZipCode as 'Postal Code', Phone, EmailAddress as 'Email' FROM Customer";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtCustomers.Load(reader);
            }
            con.Close();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtCustomers);
            return ds.Tables[0];
        }

        [WebMethod]
        public int deleteCustomer(int Id)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("DELETE from Customer WHERE (CustID='" + Id + "')", con);
            var j = cmd.ExecuteNonQuery();
            con.Close();

            return j;
        }

        [WebMethod]
        public int editCustomer(int Id, string firstName, string lastName, string address, string city, string postalCode, string country, string phone, string email)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Customer SET CustFirstName ='" + firstName + "', CustLastName ='" + lastName + "', Address ='" + address + "', City ='" + city + "', ZipCode ='" + postalCode + "', Country ='" + country + "', Phone ='" + phone + "', EmailAddress ='" + email + "' WHERE (CustID='" + Id + "')", con);
            var j = cmd.ExecuteNonQuery();
            con.Close();

            return j;
        }

        [WebMethod]
        public DataTable searchCustomer(string searchStr)
        {
            DataTable dtCustomers = new DataTable();
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string query = "SELECT * FROM Customer where CustFirstName like '" + searchStr + "%'";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtCustomers.Load(reader);
            }
            con.Close();

            DataSet ds = new DataSet();
            ds.Tables.Add(dtCustomers);
            return ds.Tables[0];
        }

        /*Nailia - Services*/
        [WebMethod]
        public DataTable getServicesList()
        {
            DataTable dtServices = new DataTable();
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string query = "SELECT * FROM Services";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtServices.Load(reader);
            }
            con.Close();

            DataSet ds = new DataSet();
            ds.Tables.Add(dtServices);
            return ds.Tables[0];
        }

        [WebMethod]
        public DataTable searchService(string searchStr)
        {
            DataTable dtServices = new DataTable();
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string query = "SELECT * FROM Services where serviceName like '" + searchStr + "%'";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtServices.Load(reader);
            }
            con.Close();

            DataSet ds = new DataSet();
            ds.Tables.Add(dtServices);
            return ds.Tables[0];
        }

        [WebMethod]
        public int editService(int Id, string serviceName, double Price)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Services SET serviceName ='" + serviceName + "', Price ='" + Price + "' WHERE (Id='" + Id + "')", con);
            var j = cmd.ExecuteNonQuery();
            con.Close();

            return j;
        }

        [WebMethod]
        public int deleteService(int Id)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("DELETE from Services WHERE (Id='" + Id + "')", con);
            var j = cmd.ExecuteNonQuery();
            con.Close();

            return j;
        }

        [WebMethod]
        public int AddNewService(string serviceName, double Price)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            cmd = new SqlCommand("INSERT INTO Services (serviceName, Price) " +
            "VALUES (@serviceName, @Price)", con);
            cmd.Parameters.AddWithValue("@serviceName", serviceName);
            cmd.Parameters.AddWithValue("@Price", Price);

            int i = cmd.ExecuteNonQuery();
            con.Close();

            return i;
        }

        [WebMethod]
        public string checkService(string serviceName, double Price)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string j = "";

            //check if the new customer already exist
            string qryCheck = "select Id from Services where serviceName = '" + serviceName + "'";
            using (SqlCommand cmd = new SqlCommand(qryCheck, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var servID = reader["Id"];
                    j = servID.ToString();
                }
            }
            return j;
        }

        /*Nailia - Doctors*/
        [WebMethod]
        public string checkDoctor(string firstname, string lastname)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string j = "";

            //check if the new doctor already exist
            string qryCheck = "select Id from Doctors where FirstName = '" + firstname + "' and LastName = '" + lastname + "'";
            using (SqlCommand cmd = new SqlCommand(qryCheck, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var docID = reader["Id"];
                    j = docID.ToString();
                }
            }
            return j;
        }

        [WebMethod]
        public int AddNewDoctor(string firstName, string lastName, string medNum)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            // con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            cmd = new SqlCommand("INSERT INTO Doctors (FirstName, LastName, MedicalIdNumber) " +
            "VALUES (@firstName, @lastName, @medNum)", con);
            cmd.Parameters.AddWithValue("@firstName", firstName);
            cmd.Parameters.AddWithValue("@lastName", lastName);
            cmd.Parameters.AddWithValue("@medNum", medNum);
            int i = cmd.ExecuteNonQuery();
            con.Close();

            return i;
        }

        [WebMethod]
        public DataTable displayDoctorsList()
        {
            DataTable dtDoctors = new DataTable();
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string query = "SELECT * FROM Doctors";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtDoctors.Load(reader);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dtDoctors);
            return ds.Tables[0];
        }

        [WebMethod]
        public int deleteDoctor(int Id)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            // con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("DELETE from Doctors WHERE (Id='" + Id + "')", con);
            var j = cmd.ExecuteNonQuery();
            con.Close();

            return j;
        }

        [WebMethod]
        public int editDoctor(int Id, string firstName, string lastName, string medNum)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Doctors SET FirstName ='" + firstName + "', LastName ='" + lastName + "', MedicalIdNumber ='" + medNum +"'WHERE (Id='" + Id + "')", con);
            var j = cmd.ExecuteNonQuery();
            con.Close();

            return j;
        }



        /*Shushan*/

        /// Receipts///      
        [WebMethod]
        public DataTable getAllReceiptsAsStrings()
        {            
            DataTable dtReceipt = new DataTable();
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string qry = "select R.ReceiptNumber, C.CustFirstName as FirstName," +
                " C.CustLastName as LastName, S.serviceName as Service, " +
                "format (S.Price, '#,###.00$') as Price," +
                " D.MedicalIdNumber, R.Date as Date from Receipt as R" +
                " inner join Customer as C on C.CustID=R.CustID " +
                "inner join Services as S on S.Id = R.ServiceID " +
                "inner join Doctors as D on D.Id=DoctorID";
            using (SqlCommand cmd = new SqlCommand(qry, con))
            {                
                SqlDataReader reader = cmd.ExecuteReader();
                dtReceipt.Load(reader);
            }
            con.Close();

            DataSet ds = new DataSet();
            ds.Tables.Add(dtReceipt);

            return ds.Tables[0];            
        }

        [WebMethod]
        public DataTable getReceiptByCustomerFirsName(string name)
        {
            DataTable dtReceipt = new DataTable();
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string qry = "select R.ReceiptNumber, C.CustFirstName as firstName, " +
                " C.CustLastName as lastName, S.serviceName as service, " +
                "S.Price as price,  D.MedicalIdNumber, R.Date from Receipt as R" +
                " inner join Customer as C on C.CustID=R.CustID " +
                "inner join Services as S on S.Id = R.ServiceID " +
                "inner join Doctors as D on D.Id=DoctorID where C.CustFirstName like '" + name + "%'";

            using (SqlCommand cmd = new SqlCommand(qry, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtReceipt.Load(reader);
            }
            con.Close();

            DataSet ds = new DataSet();
            ds.Tables.Add(dtReceipt);

            return ds.Tables[0];
        }       

        [WebMethod]
        public int deleteReceipt(int Id)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("Delete from Receipt where ReceiptNumber='" + Id + "'", con);
            int i = cmd.ExecuteNonQuery();
            con.Close();

            return i;
        }

        [WebMethod]
        public int saveReceipt(int DoctorID, int CustID, int ServiceID, DateTime Date)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
           // con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            cmd = new SqlCommand("INSERT INTO Receipt (DoctorID, CustID, ServiceID, Date) " +
                "VALUES (@doctorID, @custID, @serviceID, @date)", con);
            cmd.Parameters.AddWithValue("@doctorID", DoctorID);
            cmd.Parameters.AddWithValue("@custID", CustID);
            cmd.Parameters.AddWithValue("@serviceID", ServiceID);
            cmd.Parameters.AddWithValue("@date", Date);           

            int i = cmd.ExecuteNonQuery();
            con.Close();

            return i;

        }

        [WebMethod]
        public int updateReceipt(int ReceiptNumber, int DoctorID, int CustID, int ServiceID, DateTime Date)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            // con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Receipt SET DoctorID ='" + DoctorID + "', CustID ='" + CustID + "', ServiceID ='" + ServiceID + "', Date ='" + Date + "' WHERE (ReceiptNumber='" + ReceiptNumber + "')", con);
            int i = cmd.ExecuteNonQuery();
            con.Close();

            return i;
        }

        [WebMethod]
        public List<string> getReceiptByCustName(string CustFirstName, string CustLastName)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name", "Date" };
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByCustName", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CustFirstName", SqlDbType.NVarChar).Value = CustFirstName;
                cmd.Parameters.AddWithValue("@CustLastName", SqlDbType.NVarChar).Value = CustLastName;

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
            }
            con.Close();

            return ReceiptList;
        }

        [WebMethod]
        public List<string> getReceiptByYear(int year)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name" , "Date"};
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByYear", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Year", SqlDbType.Int).Value = year;                

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {                    
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
            }
            con.Close();

            return ReceiptList;
        }

        [WebMethod]
        public List<string> getReceiptByYearAndMonth(int year, int month)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name", "Date" };
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByYearMonth", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.AddWithValue("@Month", SqlDbType.Int).Value = month;

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
            }
            con.Close();

            return ReceiptList;
        }

        [WebMethod]
        public List<string> getReceiptByYearAndCustName(int year, string firstName, string lastName)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name", "Date" };
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByYearCustName", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.AddWithValue("@CustFirstName", SqlDbType.NVarChar).Value = firstName;
                cmd.Parameters.AddWithValue("@CustLastName", SqlDbType.NVarChar).Value = lastName;


                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
            }
            con.Close();

            return ReceiptList;
        }

        [WebMethod]
        public List<string> getReceiptByYearMonthAndName(int year,int month, string firstName, string lastName)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name", "Date" };
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByYearMonthCustName", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Year", SqlDbType.Int).Value = year;
                cmd.Parameters.AddWithValue("@Month", SqlDbType.Int).Value = month;
                cmd.Parameters.AddWithValue("@CustFirstName", SqlDbType.NVarChar).Value = firstName;
                cmd.Parameters.AddWithValue("@CustLastName", SqlDbType.NVarChar).Value = lastName;


                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
            }
            con.Close();

            return ReceiptList;
        }

        [WebMethod]
        public List<string> getReceiptByDate(DateTime date)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
           // SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name", "Date" };
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByDate", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Date", SqlDbType.Date).Value = date;
                
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
            }
            con.Close();

            return ReceiptList;
        }

        [WebMethod]
        public List<string> getReceiptByDateAndName(DateTime date, string firstName, string lastName)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name", "Date" };
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByDateCustName", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Date", SqlDbType.Date).Value = date;
                cmd.Parameters.AddWithValue("@CustFirstName", SqlDbType.NVarChar).Value = firstName;
                cmd.Parameters.AddWithValue("@CustLastName", SqlDbType.NVarChar).Value = lastName;


                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
            }
            con.Close();

            return ReceiptList;
        }

        [WebMethod]
        public List<string> getReceiptByDoctorNumId(string docNumId)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name", "Date" };
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByDoctorId", con))
           {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DocMedID", SqlDbType.NVarChar).Value = docNumId;
               
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
           }
            con.Close();

            return ReceiptList;
        }

        [WebMethod]
        public List<string> getReceiptByServiceName(string serviceName)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            List<string> ReceiptList = new List<string>();
            string[] firstRow = { "Receipt N", "Medical Id", "First Name", "Last Name", "Price", "Service Name", "Date" };
            ReceiptList.AddRange(firstRow);

            using (SqlCommand cmd = new SqlCommand("ReportByServiceName", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ServiceName", SqlDbType.NVarChar).Value = serviceName;

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    double pr = Double.Parse(reader["Price"].ToString());
                    DateTime dt = DateTime.Parse(reader["Date"].ToString());
                    string[] receipt = { reader["ReceiptNumber"].ToString(),
                        reader["MedicalIdNumber "].ToString(),
                        reader["CustFirstName"].ToString(),
                        reader["CustLastName"].ToString(),
                        pr.ToString("C"),
                        reader["serviceName"].ToString(),
                        dt.ToShortDateString()};
                    ReceiptList.AddRange(receipt);
                }
            }
            con.Close();

            return ReceiptList;
        }

        ///Serives///        
        [WebMethod]
        public List<ComboData> getAllServices()
        {
            List<ComboData> ListServiceData = new List<ComboData>();
            ListServiceData.Add(new ComboData { Id = 0, Value = "Select a service" });
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();

            SqlCommand cmd = new SqlCommand("Select Id, serviceName from Services", con);
            var reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                ListServiceData.Add(new ComboData { Id = int.Parse(reader["Id"].ToString()), Value = reader["serviceName"].ToString() });             
            }
            con.Close();

            return ListServiceData;
        }

        [WebMethod]
        public string getServicePriceById(int Id)
        {
            string price = "";
            double amount;

            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string qry = "Select Price from Services where Id='"+Id+"'";

            using (SqlCommand cmd = new SqlCommand(qry, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    amount = double.Parse(reader["Price"].ToString());
                    price = amount.ToString("C");//Mpney format
                }
            }
            con.Close();

            return price;
        }

        ///Doctor///
        [WebMethod]
        public List<ComboData> getDoctorsList()
        {
            List<ComboData> ListDoctorData = new List<ComboData>();
            ListDoctorData.Add(new ComboData { Id = 0, Value = "Select Doctor Number" });
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            
            string qry = "Select Id, MedicalIdNumber from Doctors";
            SqlCommand cmd = new SqlCommand(qry, con);
            
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ListDoctorData.Add(new ComboData { Id = int.Parse(reader["Id"].ToString()), Value = reader["MedicalIdNumber"].ToString() });
            }
            con.Close();

            return ListDoctorData;
        }

        [WebMethod]
        public int getDoctorIdByMedicalIdNumber(string MedicalIdNumber)
        {
            con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();

            int Id = 0;
            string qry = "Select Id from Doctors where MedicalIdNumber='" + MedicalIdNumber + "'";
            using (SqlCommand cmd = new SqlCommand(qry, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                     Id = int.Parse(reader["Id"].ToString());                  
                }                
            }
            con.Close();

            return Id;
        }        
    }
}
