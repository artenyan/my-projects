
create schema LibararyManagement;

use LibararyManagement;

create TABLE User (
userID int AUTO_INCREMENT NOT NULL,
username VARCHAR(20) not null, 
password VARCHAR(10) not null, 
userFirstName Varchar(40),
userLastName Varchar(40),
city varchar(20),
postalCode varchar(10),
PRIMARY KEY (userID)
);

insert into User(username, password, userFirstName, userLastName, city, postalCode) values("admin","admin", "David", "Lacroix", "Montreal", "H3W 7Y9");
insert into User(username, password, userFirstName, userLastName, city, postalCode) values("ani","ani", "Ani", "Qochar", "Montreal", "K8T 4E5");
insert into User(username, password, userFirstName, userLastName, city, postalCode) values("book","book", "Natalia", "Karchegina", "Laval", "H0F 5Q9");
insert into User(username, password, userFirstName, userLastName, city, postalCode) values("dudu","dudu", "Tamara", "Martin", "Laval", "L7E 7Y9");
insert into User(username, password, userFirstName, userLastName, city, postalCode) values("net","net", "Tina", "Jack", "Quebec", "J6S 6R5");

select *
from User;

create TABLE Book (
bookID int AUTO_INCREMENT NOT NULL ,
bookName NVARCHAR(60) not null, 
bookAuthor NVarchar(40),
PRIMARY KEY (bookID)
);

insert into Book(bookName, bookAuthor) values("Little Prince", "Antoine de saint exupéry");
insert into Book(bookName, bookAuthor) values("Desert Solitaire", "Edward Abbey");
insert into Book(bookName, bookAuthor) values("All About Love", "Bell Hooks");
insert into Book(bookName, bookAuthor) values("Disgrace", "J. M. Coetzee");
insert into Book(bookName, bookAuthor) values("Geek Love", "Katherine Dunn");
insert into Book(bookName, bookAuthor) values("Gilead", "Marilynne Robinson");
insert into Book(bookName, bookAuthor) values("Giovanni's Room", "James Baldwin");
insert into Book(bookName, bookAuthor) values("Harry Potter", "J.K. Rowling");
insert into Book(bookName, bookAuthor) values("The Merry Adventures of Robin Hood", " ‎Howard Pyle	");
insert into Book(bookName, bookAuthor) values("Gandhi", " Mohandas K. Gandhi");

select *
from Book;