 package com.LibraryManagementController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.User;
import com.LibraryManagementController.model.dao.UserDao;

public class MainController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		PrintWriter pw =response.getWriter();
		
		String userName = request.getParameter("userName");
		String password = request.getParameter("pass");
				
		UserDao userDao = new UserDao();
		
		System.out.println("UserName:"+userName);
		
		User searchedUser = userDao.getUser(userName, password);
		
		
		if (searchedUser.getUsername() == null) {
			
			pw.write("<style>\r\n" + 
					"	.lbl{\r\n" + 
					"	font-size: 18px;\r\n" + 
					"	}\r\n" + 
					"	.input1, .input2{height:30px;\r\n" + 
					"		border-radius: 4px;cursor: pointer;}\r\n" + 
					"	.input1{margin-left:5px;}\r\n" + 
					"	.input2{margin-left:13px;}\r\n" + 
					"	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px;}\r\n" + 
					"	\r\n" + 
					"</style>\r\n" + 
					"<body style=\"background-color: #eff8e7; text-align: center;\">\r\n" + 
					"<br/>\r\n" + 					
					"<h2 style=\"font-size: 30px;\">Library Management</h2>\r\n" + 
					"<br/>\r\n" + 
					"\r\n" + 
					"<form action=\"MainController\" method=\"post\">\r\n" + 
					"	<label class=\"lbl\">User Name: </label><input class=\"input1\" type=\"text\" name=\"userName\" required/>\r\n" + 
					"	<br/>\r\n" + 
					"	<br/>\r\n" + 
					"	<label class=\"lbl\">Password: </label><input  class=\"input2\" type=\"password\" name=\"pass\" required/>\r\n" + 
					"	<br/>\r\n" + 
					"	<br/>\r\n" + 
					"	<button>Sign In</button>\r\n" + 
					"</form>\r\n" + 
					"<h1 style=\"color:red;\">The user name or password isn't correct!!!</h1>\r\n" + 
					"\r\n" + 
					"</body>\r\n" + 
					"");
		}
		else {
			String passwordInput = searchedUser.getPassword();
			String usernameInput = searchedUser.getUsername();
		
			System.out.println("searchedUser"+searchedUser);
			System.out.println("passwordInput:"+passwordInput);
			System.out.println("usernameInput:"+usernameInput);
			
			if (userName.equals(usernameInput ) && password.equals(passwordInput) && passwordInput.equals("admin") ) {
				pw.write("<style>\r\n" + 
						"	body {\r\n" + 
						"		background-color:#eff8e7; \r\n" + 
						"		text-align: center;\r\n" + 
						"	}\r\n" + 
						"	.title{	font-size: 35px;}\r\n" + 
						"	.info{ text-align:left; margin-left: 42%;	}\r\n" + 
						"	body{background-color: #eff8e7; text-align: center;}\r\n" + 
						"	.input{height:30px;	border-radius: 4px;cursor: pointer;}\r\n" + 
						"	label{font-size: 22px;}\r\n" + 
						"	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px;}\r\n" + 
						"	fieldset{font-size: 22px;width: 200px;text-align: center; margin: auto; }\r\n" + 
						"	</style>" +
						"	<form action=\"index.jsp\" method=\"get\">\r\n" +
						"	<button style=\"float:right;\">Sign Out</button>\r\n" +
						"	</form>\r\n" +
						"	<form action=\"BookList\" method=\"get\">\r\n" +
						"	<button style=\"float:right;\"> Show Book List</button>\r\n" +
						"	</form>\r\n" +
						"	<form action=\"UserList\" method=\"get\">\r\n" +
						"	<button style=\"float:right;\"> Show User List</button>\r\n" +
						"	</form>\r\n" +
						"		<br/>\r\n" + 
						"		<br/>\r\n" + 
						"	<fieldset>\r\n" + 
						"	<form action=\"BookInformationAdmin\" method=\"get\">\r\n" + 						
						"		<legend>Book</legend>\r\n" + 
						"		<label>Book Name</label>\r\n" + 										
						"		<input type=\"text\" name=\"bookName\" class=\"input\" required/><br/><br/><button>Find Book</button>\r\n" + 
						"		<br/>\r\n" +						 
						"    </form>\r\n" +
						"	<form action=\"BookAddAdmin.jsp\" method=\"get\">\r\n" +
						"		<button>Add New Book</button>\r\n" + 
						"   </form>\r\n" +
						"	</fieldset>\r\n" + 
						"		<br/>\r\n" + 
						"		<br/>\r\n" + 
						"	<fieldset>\r\n" + 
						"	<form action=\"UserInformationAdmin\" method=\"get\">\r\n" +
						"		<legend>User</legend>\r\n" + 
						"		<label>User Name</label>\r\n" + 
						"		<br/>\r\n" + 
						"		<input type=\"text\" name=\"userName\"class=\"input\" required/>" +
						"	<br/><br/><button>Find User</button>\r\n" +
						"   </form>\r\n" +
						"<form action=\"UserAddAdmin.jsp\" method=\"get\">\r\n" +
						"		<button>Add New User</button>\r\n" + 						
						"	</fieldset>\r\n" + 
						"</form>"); 			
				
			}else if(userName.equals(usernameInput ) && password.equals(passwordInput)) {
				System.out.println("correct user name: "+passwordInput.equals(usernameInput ));
				pw.write("<style>\r\n" + 
						"	.input{height:30px;\r\n" + 
						"		border-radius: 4px;cursor: pointer;}\r\n" + 
						"	label{\r\n" + 
						"	font-size: 22px;}\r\n" + 
						"	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px;}\r\n" + 
						"	fieldset{\r\n" + 
						"	font-size: 22px;\r\n" + 
						"		width: 200px;\r\n" + 
						"		text-align: center;		\r\n" + 
						" 	margin: auto;"+
						"	}\r\n" + 
						"</style>\r\n" + 						
						"<body style=\" background-color: #eff8e7; text-align: center;\">\r\n" + 
						"<br/>\r\n" + 
						"<form action=\"index.jsp\" method=\"get\">\r\n" +
						" <button style=\"float:right;\">Sign Out</button>\r\n" +
						"<br/>\r\n" + 
						"</form>\r\n" +
						"	<form action=\"BookList\" method=\"get\">\r\n" +
						"	<button style=\"float:right;\"> Show Book List</button>\r\n" +
						"	</form>\r\n" +
						"<br/>\r\n" + 
						"<br/>\r\n" + 
						"<h2 style=\"font-size: 30px;\">Welcome to our Library</h2>\r\n" + 
						"<h3 >You can find your preferable book information here</h3>\r\n" + 
						"<form action=\"BookInformation\" method=\"get\">\r\n" + 
						"	<fieldset>\r\n" + 
						"		<legend class=\"title\">Book</legend>\r\n" + 
						"		<label>Book Name</label>\r\n" + 
						"		<br/>\r\n" + 
						"		<input type=\"text\" name=\"bookName\" class=\"input\" required/>\r\n" + 
						"		<br/><br/><button>Find Book</button>\r\n" + 
						"	</fieldset>\r\n" + 
						"</form>\r\n" + 
						"</body>");
				
			}else {pw.write("<style>\r\n" + 
					"	.lbl{\r\n" + 
					"	font-size: 18px;\r\n" + 
					"	}\r\n" + 
					"	.input1, .input2{height:30px;\r\n" + 
					"		border-radius: 4px;cursor: pointer;}\r\n" + 
					"	.input1{margin-left:5px;}\r\n" + 
					"	.input2{margin-left:13px;}\r\n" + 
					"	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px;}\r\n" + 
					"	\r\n" + 
					"</style>\r\n" + 
					"<body style=\"background-color: #eff8e7; text-align: center;\">\r\n" + 
					"<br/>\r\n" + 					
					"<h2 style=\"font-size: 30px;\">Library Management</h2>\r\n" + 
					"<br/>\r\n" + 
					"\r\n" + 
					"<form action=\"MainController\" method=\"post\">\r\n" + 
					"	<label class=\"lbl\">User Name: </label><input class=\"input1\" type=\"text\" name=\"userName\" required/>\r\n" + 
					"	<br/>\r\n" + 
					"	<br/>\r\n" + 
					"	<label class=\"lbl\">Password: </label><input  class=\"input2\" type=\"password\" name=\"pass\" required/>\r\n" + 
					"	<br/>\r\n" + 
					"	<br/>\r\n" + 
					"	<button>Sign In</button>\r\n" + 
					"</form>\r\n" + 
					"<h1 style=\"color:red;\">The user name or password isn't correct!!!</h1>\r\n" + 
					"\r\n" + 
					"</body>\r\n" + 
					"");
				
			}
		}
	}


}
