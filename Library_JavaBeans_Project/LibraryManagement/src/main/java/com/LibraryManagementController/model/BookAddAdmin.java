package com.LibraryManagementController.model;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.dao.BookAddDao;


public class BookAddAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");		
		String bookName = request.getParameter("bookName"); 
		String bookAuthor = request.getParameter("bookAuthor");
		
		BookAddDao bookAddDao = new BookAddDao();
		
		Book addBook = bookAddDao.getBook(bookName,  bookAuthor);
		/*String bookNameInput = searchedBook.getBookName();
		String bookAuthorInput = searchedBook.getBookAuthor();*/
		
		//user on line 29 will be used in result.jsp
		//searchedUser is object that we have  created on line 25 above
	
		System.out.println(" addBook "+ addBook);
		RequestDispatcher rd = request.getRequestDispatcher("BookAddResultAdmin.jsp");
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
