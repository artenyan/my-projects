package com.LibraryManagementController.model;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.Book;
import com.LibraryManagementController.model.dao.BookDao;

public class BookInformationAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");	
		
		String bookName = request.getParameter("bookName");
		
		BookDao bookDao = new BookDao();
		
		Book searchedBook = bookDao.getBook(bookName);
		/*String bookNameInput = searchedBook.getBookName();
		String bookAuthorInput = searchedBook.getBookAuthor();*/
		
		//user on line 29 will be used in result.jsp
		//searchedUser is object that we have  created on line 25 above
		request.setAttribute("book", searchedBook);
		
		RequestDispatcher rd = request.getRequestDispatcher("bookAdmin.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
