package com.LibraryManagementController.model;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.dao.BookListDao;


public class BookList extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");	
		
		BookListDao bookListDao = new BookListDao();
		
		ArrayList<Book> searchedBook = bookListDao.getBook();
		
		request.setAttribute("bookList", searchedBook);
		
		
		System.out.println("bookList"+ searchedBook);
		System.out.println("bookLength"+ searchedBook.size());
		RequestDispatcher rd = request.getRequestDispatcher("bookList.jsp");
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
