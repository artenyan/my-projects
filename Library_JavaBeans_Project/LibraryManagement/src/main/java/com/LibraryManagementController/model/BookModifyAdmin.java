package com.LibraryManagementController.model;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.dao.BookDao;


public class BookModifyAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");	
		
		String bookName = request.getParameter("bookName");
		
		BookDao bookDao = new BookDao();
		
		Book searchedBook = bookDao.getBook(bookName);
		
		request.setAttribute("book", searchedBook);
		
		RequestDispatcher rd = request.getRequestDispatcher("BookModifyAdmin.jsp");
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
