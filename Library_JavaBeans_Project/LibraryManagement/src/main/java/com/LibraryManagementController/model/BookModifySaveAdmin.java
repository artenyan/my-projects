package com.LibraryManagementController.model;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.dao.BookSaveDao;

public class BookModifySaveAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");	
				
		String bookID = request.getParameter("bookID");
		String bookName = request.getParameter("bookName");
		String bookAuthor = request.getParameter("bookAuthor");
		System.out.println("BookModifysave bookID" +bookID);
		BookSaveDao bookSaveDao = new BookSaveDao();
		
		Book savedBook = bookSaveDao.getBook(bookID, bookName, bookAuthor);
		
		request.setAttribute("book", savedBook);
		
		RequestDispatcher rd = request.getRequestDispatcher("BookSaveAdmin.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
