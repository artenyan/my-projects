package com.LibraryManagementController.model;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.dao.UserAddDao;


public class UserAddAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");		
		String username = request.getParameter("userName"); 
		String password = request.getParameter("password");
		String userFirstName = request.getParameter("firstName");
		String userLastName = request.getParameter("lastName");
		String city = request.getParameter("city");
		String postalCode = request.getParameter("postalCode");

		
		UserAddDao userAddDao = new UserAddDao();
		
		User addUser = userAddDao.getUser(username,  password, userFirstName,userLastName, city, postalCode );
		System.out.println(" addUser "+ addUser);
		RequestDispatcher rd = request.getRequestDispatcher("UserAddResultAdmin.jsp");
		rd.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
