package com.LibraryManagementController.model;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.dao.UserDeleteDao;


public class UserDeleteAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");	
		
		String username = request.getParameter("userName");
		
		UserDeleteDao userDeleteDao = new UserDeleteDao();
		
		User deletedUser = userDeleteDao.getUser(username);
		/*String usernameInput = searchedBook.getBookName();
		String bookAuthorInput = searchedBook.getBookAuthor();*/
		
		//user on line 29 will be used in result.jsp
		//searchedUser is object that we have  created on line 25 above
		request.setAttribute("user", deletedUser);
		
		RequestDispatcher rd = request.getRequestDispatcher("UserDeleteAdmin.jsp");
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
