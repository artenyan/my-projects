package com.LibraryManagementController.model;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.User;
import com.LibraryManagementController.model.dao.UserAdminDao;

public class UserInformationAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");	
		
		String username = request.getParameter("userName");
				
		UserAdminDao userAdminDao = new UserAdminDao();
		
		User searchedUser = userAdminDao.getUser(username);		
		
		request.setAttribute("user", searchedUser);
		
		RequestDispatcher rd = request.getRequestDispatcher("userAdmin.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
