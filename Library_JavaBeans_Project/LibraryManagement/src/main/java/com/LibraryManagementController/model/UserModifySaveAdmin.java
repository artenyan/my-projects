package com.LibraryManagementController.model;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.LibraryManagementController.model.dao.UserSaveDao;

public class UserModifySaveAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");	
		
		String userID = request.getParameter("userID");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String userFirstName = request.getParameter("userFirstName");
		String userLastName = request.getParameter("userLastName");
		String city = request.getParameter("city");
		String postalCode = request.getParameter("postalCode");
		
		System.out.println("UserModifysave userID" +userID);
		UserSaveDao userSaveDao = new UserSaveDao();
		
		User savedUser = userSaveDao.getUser(userID, userName, password, userFirstName, userLastName, city, postalCode);
		
		request.setAttribute("user", savedUser);
		
		RequestDispatcher rd = request.getRequestDispatcher("UserSaveAdmin.jsp");
		rd.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
