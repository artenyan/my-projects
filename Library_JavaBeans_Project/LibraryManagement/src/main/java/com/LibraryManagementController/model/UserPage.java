package com.LibraryManagementController.model;

public class UserPage {	
	private int userID;
	private String Username; 
	private String userFirstName;
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String Username) {
		this.Username = Username;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	@Override
	public String toString() {
		return "User [userID=" + userID + ", Username=" + Username + ", userFirstName=" + userFirstName + "]";
	}
}
