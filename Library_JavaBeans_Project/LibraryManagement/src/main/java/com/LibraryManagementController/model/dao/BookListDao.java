package com.LibraryManagementController.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.LibraryManagementController.model.Book;

public class BookListDao {
	public  ArrayList<Book> getBook()   {		
	    ArrayList<Book> listBook = new ArrayList<Book>();

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/LibararyManagement", "root", "root");
			String query = "select * from Book";
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);			
			System.out.println("rs: "+rs);
			
			while(rs.next()) {
				Book book = new Book();	
				int bookIDFromDB = rs.getInt("bookID");
				String bookNameFromDB = rs.getString("bookName");
				String bookAuthorFromDB = rs.getString("bookAuthor");
				
				book.setBookID(bookIDFromDB);
				book.setBookName(bookNameFromDB);
				book.setBookAuthor( bookAuthorFromDB);
				listBook.add(book);
			}
			con.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
		System.out.println("listBook"+listBook);
		return listBook;
	}
	
}
