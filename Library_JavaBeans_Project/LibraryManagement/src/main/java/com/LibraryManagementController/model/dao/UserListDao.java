package com.LibraryManagementController.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.LibraryManagementController.model.User;

public class UserListDao {
	public  ArrayList<User> getUser()   {		
	    ArrayList<User> listUser = new ArrayList<User>();

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/LibararyManagement", "root", "root");
			String query = "select * from User";
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);			
			System.out.println("rs: "+rs);
			
			while(rs.next()) {
				User user = new User();	
				int userIDFromDB = rs.getInt("userID");
				String usernameFromDB = rs.getString("username");
				String passwordFromDB = rs.getString("password");
				String userFirstNameFromDB = rs.getString("userFirstName");
				String userLastNameFromDB = rs.getString("userLastName");
				String cityFromDB = rs.getString("city");				
				String postalCodeFromDB = rs.getString("postalCode");			
				
				user.setUserID(userIDFromDB);
				user.setUsername(usernameFromDB);
				user.setPassword(passwordFromDB);
				user.setUserFirstName(userFirstNameFromDB);			
				user.setUserLastName(userLastNameFromDB);
				user.setCity(cityFromDB);
				user.setPostalCode(postalCodeFromDB);
				listUser.add(user);
			}
			con.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
		System.out.println("listBook"+listUser);
		return listUser;
	}
}
