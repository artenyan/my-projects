<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete Book</title>
</head>
<style>	
	.title{	font-size: 35px;}
	.info{ text-align:left; margin-left:45%;}
	body{background-color: #eff8e7; text-align: center;}
	.input{height:30px;	border-radius: 4px;cursor: pointer;}
	label{font-size: 22px;}
	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px; }
	fieldset{font-size: 22px;width: 200px;text-align: center; margin:auto;}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>	

<body>
<form action="index.jsp" method="get">
 	<button style="float:right; background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px; width: 150px;">Sign Out</button>
</form>	
<button style="float:left;" onclick="goBack()">Back</button> 
<h1>Book Deleted successfully</h1>
</body>
</html>