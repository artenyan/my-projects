<%@page import="com.LibraryManagementController.model.Book"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Book Information</title>
</head>
<style>
	body {
		background-color:#eff8e7; 
		text-align: center;
	}
	
	.info{ text-align:left; margin-left: 44%;	}
	body{background-color: #eff8e7; text-align: center;}
	.input{height:30px;	border-radius: 4px;cursor: pointer;}
	label{font-size: 22px;}
	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px; width: 150px;}
	fieldset{font-size: 22px;width: 200px;text-align: center;}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>	
		
<body>
	<% Book book = (Book)request.getAttribute("book");	%> 
	<form action="index.jsp" method="get">
	 	<button style="float:right;">Sign Out</button>
	</form>	
	<button style="float:left;" onclick="goBack()">Back</button> 
	<br/>
	<br/>
	<br/>
	<div class="info"> 
	<h1 >Modified Book</h1>
	 	<br/>
	 	<br/>
	 	<br/>	
	 		
	 	<form action="BookModifySaveAdmin" method="get">		 	
			<label>Book Name: </label> 	
			<label><%= book.getBookName() %></label> 				
			<br/>		
			<input type="text" name="bookName" class="input">
			<br/>
			<br/>			
			<label>Book Author:</label>
			<label><%= book.getBookAuthor() %></label> 				
			<br/>
			<input type="text" name="bookAuthor" class="input" required/> 		
			<br/>
			<br/>			
			<button>Save</button>
			<input type="hidden" name="bookID" value="<%=book.getBookID() %>"/>
		</form>
	</div>
		
</body>
</html>