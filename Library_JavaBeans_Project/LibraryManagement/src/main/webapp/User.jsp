<%@page import="com.LibraryManagementController.model.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Information</title>
</head>
<style>
	body {
		background-color:#eff8e7; 
		text-align: center;
	}
	.title{	font-size: 35px;}
	.info{ text-align:left; margin-left: 42%;	}
	body{background-color: #eff8e7; text-align: center;}
	.input{height:30px;	border-radius: 4px;cursor: pointer;}
	label{font-size: 22px;}
	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px;}
	fieldset{font-size: 22px;width: 200px;text-align: center;}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>						
<body>	
	<% User user = (User)request.getAttribute("user");	
	String userName = user.getUsername();
	if( userName == null) {%>
		<br/> 
		<form action="index.jsp" method="get">
		 	<button style="float:right;">Sign Out</button>
		</form>	 												
		
		<form action="UserInformation" method="get">
			<fieldset>
				<legend>User</legend>
					<label>User Name</label><br/>
					<input type="text" name="userName" class="input" required/>
					<button>Find</button>
			</fieldset>	
			<h3 style="color:red">We don't have the user that you are searching, Sorry!!!</h3>				
		</form>		
	 <% }else{ %>
	 	<div class= "title">Founded User:</div>
	 	<br/>
	 	<br/>
	 	<div class="info">
		<label>User Name: </label> 		
		<label ><%= user.getUsername() %></label>
		<br/>
		<label>User First Name:</label>
		<label><%= user.getUserFirstName() %></label>		
		</div>
	<% } %>	 
</body>
</html>