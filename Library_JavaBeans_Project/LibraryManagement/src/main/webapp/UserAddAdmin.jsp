<%@page import="com.LibraryManagementController.model.Book"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Information</title>
</head>
<style>
	body {
		background-color:#eff8e7; 
		text-align: center;
	}
	.title{	font-size: 35px;}
	.info{ text-align:left; margin-left: 44%;	}
	body{background-color: #eff8e7; text-align: center;}
	.input{height:30px;	border-radius: 4px;cursor: pointer;}
	label{font-size: 22px;}
	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px; width: 150px;}
	fieldset{font-size: 22px;width: 200px;text-align: center;}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>	
	
<body>	
	<button style="float:left;" onclick="goBack()">Back</button>
	<form action="index.jsp" method="get">
		 	<button style="float:right;">Sign Out</button>
	</form>	 	
	<h1 class= "title">Add New User:</h1>
	 	<br/>
	 	<br/>
	 	<br/>	 	
	 	<form action="UserAddAdmin" method="get">
		 	<div class="info">
				<label>User Name: </label> 								
				<br/>		
				<input type="text" name="userName" class="input" required/>
				<br/>
				<br/>			
				<label>Password:</label>								
				<br/>
				<input type="text" name="password" class="input" required/> 
				<br/>			
				<label>First Name:</label>								
				<br/>
				<input type="text" name="firstName" class="input"/> 
				<br/>			
				<label>Last Name:</label>								
				<br/>
				<input type="text" name="lastName" class="input"/> 
				<br/>			
				<label>City:</label>								
				<br/>
				<input type="text" name="city" class="input"/> 
				<br/>			
				<label>Postal Code:</label>								
				<br/>
				<input type="text" name="postalCode" class="input"/> 
			</div>
			<br/>
			<br/>			
			<button>Save</button>	
		</form>
		
</body>
</html>