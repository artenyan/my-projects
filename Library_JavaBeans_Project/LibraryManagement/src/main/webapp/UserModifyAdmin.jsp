<%@page import="com.LibraryManagementController.model.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Information</title>
</head>
<style>
	body {
		background-color:#eff8e7; 
		text-align: center;
	}
	.title{	font-size: 35px;}
	.info{ text-align:left; margin-left: 40%;	}
	body{background-color: #eff8e7; text-align: center;}
	.input{height:30px;	border-radius: 4px;cursor: pointer;}
	label{font-size: 22px;}
	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px; width: 150px;}
	fieldset{font-size: 22px;width: 200px;text-align: center;}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>	
		
<body>
	<% User user = (User)request.getAttribute("user");	%> 
	<form action="index.jsp" method="get">
	 	<button style="float:right;">Sign Out</button>
	</form>	
	<button style="float:left;" onclick="goBack()">Back</button>
	<br/>
	<br/>
	<br/> 
	<div class="info">
	<h1 class= "title">Modified User</h1>
 	<br/>
 	<br/>
 	<br/>	 	
 	<form action="UserModifySaveAdmin" >	 
		<label>User Name: </label> 	
		<label><%= user.getUsername() %></label> 				
		<br/>		
		<input type="text" name="userName" class="input" required/>
		<br/>		
		<label>Password:</label>
		<label><%= user.getPassword() %></label> 				
		<br/>
		<input type="text" name="password" class="input" required/> 
		<br/>		
		<label>First Name:</label>
		<label><%= user.getUserFirstName() %></label> 				
		<br/>
		<input type="text" name="userFirstName" class="input"/> 
		<br/>		
		<label>Last Name:</label>
		<label><%= user.getUserLastName() %></label> 				
		<br/>
		<input type="text" name="userLastName" class="input"/> 
		<br/>		
		<label>City:</label>
		<label><%= user.getCity() %></label> 				
		<br/>
		<input type="text" name="city" class="input"/> 
		<br/>		
		<label>Postal Code:</label>
		<label><%= user.getPostalCode() %></label> 				
		<br/>
		<input type="text" name="postalCode" class="input"/> 
		<input type="hidden" name="userID" value="<%= user.getUserID() %>"/>	
		<br/>
		<br/>			
		<button>Save</button>		
	</form>
	</div>		
</body>
</html>