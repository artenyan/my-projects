<%@page import="com.LibraryManagementController.model.Book"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Book Information</title>
</head>
<style>
	body {
		background-color:#eff8e7; 
		text-align: center;
	}
	.title{	font-size: 35px;}
	.info{ text-align:left;}
	body{background-color: #eff8e7; text-align: center;}
	.input{height:30px;	border-radius: 4px;cursor: pointer;}
	label{font-size: 22px;}
	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px; }
	fieldset{font-size: 22px;width: 200px;text-align: center; margin:auto;}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>	
<body>	
	<% Book book = (Book)request.getAttribute("book");	
	String bookName = book.getBookName(); 
	if( bookName == null) {%>
		<br/>
		<form action="index.jsp" method="get">
		 	<button style="float:right;">Sign Out</button>
		</form>
		<form action="BookList" method="get">
			<button style="float:right;"> Show Book List</button>
		</form>	
		<form action="UserList" method="get">
			<button style="float:right;"> Show User List</button>
		</form>	
			<button style="float:left;" onclick="goBack()">Back</button>			
		<br/>
		<br/>
		<div class="info" style=" text-align: center; display: block;">
			<fieldset>
				<legend>Book</legend>
				<form action="BookInformationAdmin" method="get">				
					<label>Book Name</label><br/>
					<input type="text" name="bookName" class="input" required/>
					<br/>
					<br/>
					<button>Find Book</button>
					<br/>
					<br/>
				</form>
				<form action="BookAddAdmin.jsp" method="get">
					<button>Add New Book</button>
				</form>
			</fieldset>	
			<fieldset>
				<legend>User</legend>
				<form action="UserInformationAdmin" method="get">
					<label>User Name</label><br/>
					<input type="text" name="userName" class="input" required/>
					<br/>
					<br/>
					<button>Find User</button>
					<br/>
					<br/>
				</form>
				<form action="UserAddAdmin.jsp" method="get">
					<button>Add New User</button>				
				</form>
			</fieldset>	
			
			<h3 style="color:red; text-align: center;">In our list you don't have book in that name!!!</h3>	
		</div>			
	 <% }else{ %>
	 	<form action="index.jsp" method="get">
		 	<button style="float:right;">Sign Out</button>
		</form>	
		<button style="float:left;" onclick="goBack()">Back</button>
		<br/><br/><br/><br/>
		
	 	<label class= "title">Founded Book:</label>
	 	<br/>
	 	<br/>
	 	<br/>	 		 	
	 	<form action="BookModifyAdmin" method="get">		 	
			<label>Book Name: </label> 		
			<label><%= book.getBookName() %></label>
			<input type="hidden" name="bookName" value="<%= book.getBookName() %>"/>
			<br/>			
			<label>Book Author:</label>
			<label><%= book.getBookAuthor() %></label>				
			<br/>
			<br/>			
			<button style="width: 150px;">Modify</button>	
		</form>
		<form action="BookDeleteAdmin" method="get">			
			<br/>
			<button style="width: 150px;">Delete</button>
			<input type="hidden" name="bookName" value="<%= book.getBookName() %>"/>
		</form>		
	<% } %>	 
</body>
</html>