<%@page import="com.LibraryManagementController.model.Book"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Book Information</title>
</head>
<style>
	body {
		background-color:#eff8e7; 
		text-align: center;
	}
	.title{	font-size: 35px;}
	.info{ text-align:left; margin-left: 39%;	}
	body{background-color: #eff8e7; text-align: center;}
	.input{height:30px;	border-radius: 4px;cursor: pointer;}
	label{font-size: 22px;}
	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px;}
	fieldset{font-size: 22px;width: 200px;text-align: center; margin: auto;}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>	
					
<body>	
	<% Book book = (Book)request.getAttribute("book");	
	String bookName = book.getBookName();
	if( bookName == null) {%>
		<br/>
		<form action="index.jsp" method="get">
		 	<button style="float:right;">Sign Out</button>
		</form>	
		<form action="BookList" method="get">
			<button style="float:right;"> Show Book List</button>
		</form>				
		<button style="float:left;" onclick="goBack()">Back</button>			
		<br/>
		<br/>	
		<br/>
		<br/>	  												
		<h2 style="font-size: 30px;">Welcome to our Library</h2> 
		<h3 >You can find your preferable book information here</h3> 
		<form action="BookInformation" method="get">
			<fieldset>
				<legend>Book</legend>
					<label>Book Name</label><br/>
					<input type="text" name="bookName" class="input" required/>
					<br/>
					<button>Find Book</button>
			</fieldset>	
			<h3 style="color:red">We don't have the book that you are searching, Sorry!!!</h3>				
		</form>		
	 <% }else{ %>
	 	<form action="index.jsp" method="get">
		 	<button style="float:right;">Sign Out</button>
		</form>
		<button style="float:left;" onclick="goBack()">Back</button>
		<br/><br/>
		<div class="info">
	 	<div class= "title">Founded Book</div>
	 	<br/>
	 	<br/>	 		 	
		<label>Book Name: </label> 		
		<label ><%= book.getBookName() %></label>
		<br/>
		<label>Book Author:</label>
		<label><%= book.getBookAuthor() %></label>		
		</div>
	<% } %>	 
</body>
</html>