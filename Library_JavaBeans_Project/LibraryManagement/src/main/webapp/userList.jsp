<%@page import="com.LibraryManagementController.model.User"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Book Information</title>
</head>
<style>
	body {
		background-color:#eff8e7; 
		text-align: center;
	}
	.title{	font-size: 35px;}
	.info{ text-align:left; margin-left: 38%;	}
	body{background-color: #eff8e7; text-align: center;}
	.input{height:30px;	border-radius: 4px;cursor: pointer;}
	label{font-size: 22px;}
	button{background-color: #4CAF50;border-radius: 8px; color:white; font-size: 20px; width: 150px;}
	fieldset{font-size: 22px;width: 200px;text-align: center;}
	table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: auto;
	}
	
	td, th {
	    border: 1px solid #dddddd;
	    text-align: left;
	    padding: 8px;
	}
	
	tr:nth-child(even) {
	    background-color: #dddddd;
	}
</style>
<script>
function goBack() {
    window.history.back();
}
</script>		
<body>
	<%! ArrayList<User> userList = new ArrayList<User>(); %>
	<% userList = (ArrayList<User>)request.getAttribute("userList"); %> 
	<form action="index.jsp" method="get">
	 	<button style="float:right;">Sign Out</button>
	</form>	 	
	<button style="float:left;" onclick="goBack()">Back</button>	
	<br/>
	<br/>
	<h1 class= "title">User List</h1>
	 	<br/>
	 	<br/>
	 	<br/>	
	 	<div class="info"> 		 	
	 	<table>	
	 		<tr>
	 			<td>User ID</td>	
	 			<td>User username</td>	
	 			<td>User password</td>
	 			<td>User FirstName</td>
	 			<td>User LastName</td>
	 			<td>City</td>
	 			<td>Postal Code</td>		
			</tr>
			<% for(int i=0; i< userList.size(); i++){%>
			<tr>
				<td><%= userList.get(i).getUserID() %></td>
				<td><%= userList.get(i).getUsername() %></td>
				<td><%= userList.get(i).getPassword() %></td>
				<td><%= userList.get(i).getUserFirstName() %></td>
				<td><%= userList.get(i).getUserLastName() %></td>
				<td><%= userList.get(i).getCity() %></td>
				<td><%= userList.get(i).getPostalCode() %></td>
			</tr>	
			<%} %>
		</table> 
		
		</div>
		
</body>
</html>