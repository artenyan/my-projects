﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CustomerManagementEntities;

namespace CustomerManagement
{
    public partial class CustomerManagement : Form
    {
        private Customer currentCustomer = null;
        public CustomerManagement()
        {
            InitializeComponent();
        }

        private void populateUI()
        {
            txtCustomerFirstName.Text = currentCustomer.FirstName;
            txtCustomerLastName.Text = currentCustomer.LastName;
            txtAddress.Text = currentCustomer.Address;
            txtZipCode.Text = currentCustomer.ZipCode;
            txtCity.Text = currentCustomer.City;
            txtState.Text = currentCustomer.State;
            combCountry.SelectedValue = currentCustomer.CountryID;
            txtPhone.Text = currentCustomer.Phone;
            txtEmailAddress.Text = currentCustomer.EmailAddress;
            txtWebAddress.Text = currentCustomer.WebAddress;
            txtCreditLimit.Text = currentCustomer.CreditLimit;
            chbNewsSubscriber.Checked = currentCustomer.NewSubscriber;
        }
                    
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            Customer currentCustomer = new Customer();
            populateUI();
        }
    }
}
