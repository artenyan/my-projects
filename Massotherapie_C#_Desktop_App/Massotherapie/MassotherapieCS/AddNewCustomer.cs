﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace MassotherapieCS
{
    public partial class AddNewCustomer : Form
    {
        public AddNewCustomer()
        {
            InitializeComponent();
        }

        private void btnAddNewCust_Click(object sender, EventArgs e)
        {
            /////////////////
            //Validation part
            /////////////////

            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            //checking if required fields are empty or not
            if (txtCustFirstName.Text == "" || txtCustLastName.Text == "")
            {
                MessageBox.Show("You need to fullfill all requared field", "Error Message", MessageBoxButtons.OK);
            }
            //First Name and Last Name must be contain only letters
            else if (!Regex.IsMatch(txtCustFirstName.Text, @"^[\p{L}]+$")
                    || !Regex.IsMatch(txtCustLastName.Text, @"^[\p{L}]+$"))
            {
                MessageBox.Show("First Name and Last Name must be contain only letters", "Error Message", MessageBoxButtons.OK);
            }
            // Email address must be valid one
            else if (txtEmailAddress.Text != "" & !regex.IsMatch(txtEmailAddress.Text))
            {
                MessageBox.Show("Please enter a valid email address", "Error Message", MessageBoxButtons.OK);
            }
            //Phone number must be in required format
            else if (txtPhone.Text != "" & !Regex.IsMatch(txtPhone.Text, "\\p{N}{3}-\\p{N}{3}-\\p{N}{4}\\b"))
            {
                MessageBox.Show("Phone number must be in this form 111-111-1111", "Error Message", MessageBoxButtons.OK);
            }
            else
            {
                //save information in database
                SqlConnection connection = new SqlConnection("Data Source=localhost;Initial Catalog=Massotherapie;Integrated Security=True");
                var connectionStr = ConfigurationManager.ConnectionStrings["MyDb"].ConnectionString;

                //SqlConnection
                using (var sqlConnection = new SqlConnection(connectionStr))
                {
                    try
                    {
                        sqlConnection.Open();
                        //check if the new customer already exist
                        string qryCheck = "select CustID from Customer where CustFirstName = '" + txtCustFirstName.Text.ToString() + "' and CustLastName = '" + txtCustLastName.Text.ToString() + "'";
                        SqlCommand cmdCheck = new SqlCommand(qryCheck, sqlConnection);
                        var reader = cmdCheck.ExecuteReader();
                        while (reader.Read())
                        {
                            var custID = reader["CustID"];
                            lblCustID.Text = custID.ToString();                           
                        }
                        sqlConnection.Close();
                        sqlConnection.Open();
                        if (lblCustID.Text == "")
                        {
                            //Inserting information into Customer table
                            string qry = "insert into Customer(CustfirstName, CustLastName, Address,City, ZipCode, Country, Phone, EmailAddress) values(@p1,@p2,@p3, @p4, @p5, @p6, @p7, @p8)";

                            using (SqlCommand cmd = new SqlCommand(qry, sqlConnection))
                            {
                                cmd.Parameters.AddWithValue("@p1", txtCustFirstName.Text);
                                cmd.Parameters.AddWithValue("@p2", txtCustLastName.Text);
                                cmd.Parameters.AddWithValue("@p3", txtAddress.Text);
                                cmd.Parameters.AddWithValue("@p4", txtCity.Text);
                                cmd.Parameters.AddWithValue("@p5", txtZipCode.Text);
                                cmd.Parameters.AddWithValue("@p6", txtCountry.Text);
                                cmd.Parameters.AddWithValue("@p7", txtPhone.Text);
                                cmd.Parameters.AddWithValue("@p8", txtEmailAddress.Text);

                                int i = cmd.ExecuteNonQuery();
                                if (i >= 1)
                                {
                                    string message = i + " Customer information was saved successfully";
                                    string caption = "Message";
                                    MessageBoxButtons button = MessageBoxButtons.OK;
                                    DialogResult result;

                                    // Displays the MessageBox.
                                    result = MessageBox.Show(message, caption, button);

                                    if (result == DialogResult.OK)
                                    {
                                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                                        result = MessageBox.Show("Do you want to add the other Customer", "Message", buttons);
                                        if (result == DialogResult.Yes)
                                        {
                                            this.Show();
                                            txtCustFirstName.Text = "";
                                            txtCustLastName.Text = "";
                                            txtAddress.Text = "";
                                            txtZipCode.Text = "";
                                            txtCity.Text = "";
                                            txtCountry.Text = "";
                                            txtPhone.Text = "";
                                            txtEmailAddress.Text = "";
                                        }
                                        else
                                        {
                                            new Menu().Show();
                                            this.Hide();
                                        }
                                    }

                                }
                            }//end using
                        }
                        else
                        {
                            sqlConnection.Close();
                            sqlConnection.Open();
                            //message
                            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                            var result = MessageBox.Show("You already have the customer with the same name, you want to add one more time", "Warning Message", buttons);
                            if (result == DialogResult.Yes)
                            {
                                //Inserting information into Customer table
                                string qry = "insert into Customer(CustfirstName, CustLastName, Address,City, ZipCode, Country, Phone, EmailAddress) values(@p1,@p2,@p3, @p4, @p5, @p6, @p7, @p8)";

                                using (SqlCommand cmd = new SqlCommand(qry, sqlConnection))
                                {
                                    cmd.Parameters.AddWithValue("@p1", txtCustFirstName.Text);
                                    cmd.Parameters.AddWithValue("@p2", txtCustLastName.Text);
                                    cmd.Parameters.AddWithValue("@p3", txtAddress.Text);
                                    cmd.Parameters.AddWithValue("@p4", txtCity.Text);
                                    cmd.Parameters.AddWithValue("@p5", txtZipCode.Text);
                                    cmd.Parameters.AddWithValue("@p6", txtCountry.Text);
                                    cmd.Parameters.AddWithValue("@p7", txtPhone.Text);
                                    cmd.Parameters.AddWithValue("@p8", txtEmailAddress.Text);

                                    int i = cmd.ExecuteNonQuery();
                                    if (i >= 1)
                                    {
                                        string message = i + " Customer information was saved successfully";
                                        string caption = "Message";
                                        MessageBoxButtons button = MessageBoxButtons.OK;
                                        //DialogResult result;

                                        // Displays the MessageBox.
                                        result = MessageBox.Show(message, caption, button);

                                        if (result == DialogResult.OK)
                                        {
                                            //MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                                            result = MessageBox.Show("Do you want to add the other Customer", "Message", buttons);
                                            if (result == DialogResult.Yes)
                                            {
                                                this.Show();
                                                txtCustFirstName.Text = "";
                                                txtCustLastName.Text = "";
                                                txtAddress.Text = "";
                                                txtZipCode.Text = "";
                                                txtCity.Text = "";
                                                txtCountry.Text = "";
                                                txtPhone.Text = "";
                                                txtEmailAddress.Text = "";
                                            }
                                            else
                                            {
                                                new Menu().Show();
                                                this.Hide();
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Customer is not Added", "Message");
                                        }
                                    }
                                }
                            }
                            sqlConnection.Close();
                        }                  

                    }
                    catch (System.Exception exep)
                    {
                        MessageBox.Show("Error is " + exep.ToString());
                    }

                }//end using               
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            string message = " By canceling you will loose all inputed information";
            string caption = "Message";
            MessageBoxButtons button = MessageBoxButtons.OKCancel;
            DialogResult result;

            // Displays the MessageBox.
            result = MessageBox.Show(message, caption, button);

            if (result == DialogResult.OK)
            {
                this.Hide();
                new Menu().Show();
            }
        }

        private void btnNewOrReset_Click(object sender, EventArgs e)
        {
            string message = " By reseting you will loose all inputed information";
            string caption = "Message";
            MessageBoxButtons button = MessageBoxButtons.OKCancel;
            DialogResult result;

            // Displays the MessageBox.
            result = MessageBox.Show(message, caption, button);

            if (result == DialogResult.OK)
            {
                txtCustFirstName.Text = "";
                txtCustLastName.Text = "";
                txtAddress.Text = "";
                txtZipCode.Text = "";
                txtCity.Text = "";
                txtCountry.Text = "";
                txtPhone.Text = "";
                txtEmailAddress.Text = "";
            }
        }       
    }
}
