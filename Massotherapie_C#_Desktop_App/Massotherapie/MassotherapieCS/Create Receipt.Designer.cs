﻿namespace MassotherapieCS
{
    partial class CreateRecite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCreateRecite = new System.Windows.Forms.Button();
            this.txtCustLastNameRecite = new System.Windows.Forms.TextBox();
            this.lblCustLastNameReport = new System.Windows.Forms.Label();
            this.txtCustFirstNameRecite = new System.Windows.Forms.TextBox();
            this.lblCustFirstNameReport = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.txtAmountRecite = new System.Windows.Forms.TextBox();
            this.dateTimePickerDateRecit = new System.Windows.Forms.DateTimePicker();
            this.lblDateReport = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCustIDRecite = new System.Windows.Forms.Label();
            this.lblMatriculeNumber = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCustName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MassotherapieCS.Properties.Resources.LogoMassotherapie0;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(225, 110);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Indigo;
            this.label1.Location = new System.Drawing.Point(19, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "Gegham Martirosyan";
            // 
            // btnCancel
            // 
            this.btnCancel.AutoEllipsis = true;
            this.btnCancel.BackColor = System.Drawing.Color.PeachPuff;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnCancel.Location = new System.Drawing.Point(605, 352);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(145, 61);
            this.btnCancel.TabIndex = 53;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnCreateRecite
            // 
            this.btnCreateRecite.AutoEllipsis = true;
            this.btnCreateRecite.BackColor = System.Drawing.Color.SteelBlue;
            this.btnCreateRecite.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCreateRecite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnCreateRecite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnCreateRecite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateRecite.ForeColor = System.Drawing.Color.BurlyWood;
            this.btnCreateRecite.Location = new System.Drawing.Point(389, 352);
            this.btnCreateRecite.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCreateRecite.Name = "btnCreateRecite";
            this.btnCreateRecite.Size = new System.Drawing.Size(145, 61);
            this.btnCreateRecite.TabIndex = 52;
            this.btnCreateRecite.Text = "Create Receipt";
            this.btnCreateRecite.UseVisualStyleBackColor = false;
            this.btnCreateRecite.Click += new System.EventHandler(this.btnCreateRecite_Click);
            // 
            // txtCustLastNameRecite
            // 
            this.txtCustLastNameRecite.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustLastNameRecite.ForeColor = System.Drawing.Color.SaddleBrown;
            this.txtCustLastNameRecite.Location = new System.Drawing.Point(512, 134);
            this.txtCustLastNameRecite.Multiline = true;
            this.txtCustLastNameRecite.Name = "txtCustLastNameRecite";
            this.txtCustLastNameRecite.Size = new System.Drawing.Size(238, 47);
            this.txtCustLastNameRecite.TabIndex = 57;
            // 
            // lblCustLastNameReport
            // 
            this.lblCustLastNameReport.AutoSize = true;
            this.lblCustLastNameReport.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustLastNameReport.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCustLastNameReport.Location = new System.Drawing.Point(262, 134);
            this.lblCustLastNameReport.Name = "lblCustLastNameReport";
            this.lblCustLastNameReport.Size = new System.Drawing.Size(223, 28);
            this.lblCustLastNameReport.TabIndex = 56;
            this.lblCustLastNameReport.Text = "Customer Last Name:";
            // 
            // txtCustFirstNameRecite
            // 
            this.txtCustFirstNameRecite.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustFirstNameRecite.ForeColor = System.Drawing.Color.SaddleBrown;
            this.txtCustFirstNameRecite.Location = new System.Drawing.Point(512, 63);
            this.txtCustFirstNameRecite.Multiline = true;
            this.txtCustFirstNameRecite.Name = "txtCustFirstNameRecite";
            this.txtCustFirstNameRecite.Size = new System.Drawing.Size(238, 47);
            this.txtCustFirstNameRecite.TabIndex = 55;
            // 
            // lblCustFirstNameReport
            // 
            this.lblCustFirstNameReport.AutoSize = true;
            this.lblCustFirstNameReport.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustFirstNameReport.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCustFirstNameReport.Location = new System.Drawing.Point(262, 63);
            this.lblCustFirstNameReport.Name = "lblCustFirstNameReport";
            this.lblCustFirstNameReport.Size = new System.Drawing.Size(227, 28);
            this.lblCustFirstNameReport.TabIndex = 54;
            this.lblCustFirstNameReport.Text = "Customer First Name:";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblAmount.Location = new System.Drawing.Point(386, 205);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(99, 28);
            this.lblAmount.TabIndex = 58;
            this.lblAmount.Text = "Amount:";
            // 
            // txtAmountRecite
            // 
            this.txtAmountRecite.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountRecite.ForeColor = System.Drawing.Color.SaddleBrown;
            this.txtAmountRecite.Location = new System.Drawing.Point(512, 205);
            this.txtAmountRecite.Multiline = true;
            this.txtAmountRecite.Name = "txtAmountRecite";
            this.txtAmountRecite.Size = new System.Drawing.Size(238, 47);
            this.txtAmountRecite.TabIndex = 59;
            // 
            // dateTimePickerDateRecit
            // 
            this.dateTimePickerDateRecit.CalendarForeColor = System.Drawing.Color.SteelBlue;
            this.dateTimePickerDateRecit.CalendarTitleForeColor = System.Drawing.Color.SaddleBrown;
            this.dateTimePickerDateRecit.CalendarTrailingForeColor = System.Drawing.Color.SteelBlue;
            this.dateTimePickerDateRecit.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDateRecit.Location = new System.Drawing.Point(512, 276);
            this.dateTimePickerDateRecit.Name = "dateTimePickerDateRecit";
            this.dateTimePickerDateRecit.Size = new System.Drawing.Size(238, 26);
            this.dateTimePickerDateRecit.TabIndex = 61;
            this.dateTimePickerDateRecit.Value = new System.DateTime(2018, 5, 8, 16, 47, 10, 0);
            // 
            // lblDateReport
            // 
            this.lblDateReport.AutoSize = true;
            this.lblDateReport.Font = new System.Drawing.Font("Segoe UI Black", 10F);
            this.lblDateReport.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblDateReport.Location = new System.Drawing.Point(422, 276);
            this.lblDateReport.Name = "lblDateReport";
            this.lblDateReport.Size = new System.Drawing.Size(63, 28);
            this.lblDateReport.TabIndex = 60;
            this.lblDateReport.Text = "Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Olive;
            this.label2.Location = new System.Drawing.Point(12, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 24);
            this.label2.TabIndex = 62;
            this.label2.Text = "Address: ";
            // 
            // lblCustIDRecite
            // 
            this.lblCustIDRecite.AutoSize = true;
            this.lblCustIDRecite.Enabled = false;
            this.lblCustIDRecite.Location = new System.Drawing.Point(551, 23);
            this.lblCustIDRecite.Name = "lblCustIDRecite";
            this.lblCustIDRecite.Size = new System.Drawing.Size(0, 20);
            this.lblCustIDRecite.TabIndex = 0;
            this.lblCustIDRecite.Visible = false;
            // 
            // lblMatriculeNumber
            // 
            this.lblMatriculeNumber.AutoSize = true;
            this.lblMatriculeNumber.Enabled = false;
            this.lblMatriculeNumber.Location = new System.Drawing.Point(12, 421);
            this.lblMatriculeNumber.Name = "lblMatriculeNumber";
            this.lblMatriculeNumber.Size = new System.Drawing.Size(0, 20);
            this.lblMatriculeNumber.TabIndex = 63;
            this.lblMatriculeNumber.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Olive;
            this.label3.Location = new System.Drawing.Point(97, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 24);
            this.label3.TabIndex = 62;
            this.label3.Text = "193 Denonville";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Olive;
            this.label4.Location = new System.Drawing.Point(97, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 24);
            this.label4.TabIndex = 62;
            this.label4.Text = "Laval, QC";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.ForestGreen;
            this.label5.Location = new System.Drawing.Point(60, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 24);
            this.label5.TabIndex = 62;
            this.label5.Text = "Tel:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.ForestGreen;
            this.label6.Location = new System.Drawing.Point(97, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 24);
            this.label6.TabIndex = 62;
            this.label6.Text = "514 226 0626";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Olive;
            this.label7.Location = new System.Drawing.Point(97, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 24);
            this.label7.TabIndex = 62;
            this.label7.Text = "H7W 2M7";
            // 
            // lblCustName
            // 
            this.lblCustName.AutoSize = true;
            this.lblCustName.Enabled = false;
            this.lblCustName.Location = new System.Drawing.Point(657, 23);
            this.lblCustName.Name = "lblCustName";
            this.lblCustName.Size = new System.Drawing.Size(0, 20);
            this.lblCustName.TabIndex = 0;
            this.lblCustName.Visible = false;
            // 
            // CreateRecite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OldLace;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblMatriculeNumber);
            this.Controls.Add(this.lblCustName);
            this.Controls.Add(this.lblCustIDRecite);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePickerDateRecit);
            this.Controls.Add(this.lblDateReport);
            this.Controls.Add(this.txtAmountRecite);
            this.Controls.Add(this.lblAmount);
            this.Controls.Add(this.txtCustLastNameRecite);
            this.Controls.Add(this.lblCustLastNameReport);
            this.Controls.Add(this.txtCustFirstNameRecite);
            this.Controls.Add(this.lblCustFirstNameReport);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreateRecite);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CreateRecite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Receipt";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCreateRecite;
        private System.Windows.Forms.TextBox txtCustLastNameRecite;
        private System.Windows.Forms.Label lblCustLastNameReport;
        private System.Windows.Forms.TextBox txtCustFirstNameRecite;
        private System.Windows.Forms.Label lblCustFirstNameReport;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.TextBox txtAmountRecite;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateRecit;
        private System.Windows.Forms.Label lblDateReport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCustIDRecite;
        private System.Windows.Forms.Label lblMatriculeNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblCustName;
    }
}