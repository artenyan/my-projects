﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MassotherapieCS
{
    public partial class CreateRecite : Form
    {
       

        public CreateRecite()
        {
            InitializeComponent();
        }

        private void btnCreateRecite_Click(object sender, EventArgs e)
        {
            /////////////////
            //Validation part
            /////////////////

            //checking if required fields are empty or not
            if (txtCustFirstNameRecite.Text == "" || txtCustLastNameRecite.Text == "" || txtAmountRecite.Text == "")
            {
                MessageBox.Show("You need to fullfill First Name, Last Name and Amount field", "Error Message", MessageBoxButtons.OK);
            }
            else
            {
                //First Name and Last Name must be contain only letters
                if (!Regex.IsMatch(txtCustFirstNameRecite.Text, @"^[\p{L}]+$")
                    || !Regex.IsMatch(txtCustLastNameRecite.Text, @"^[\p{L}]+$"))
                {
                    MessageBox.Show("First Name and Last Name must be contain only letters", "Error Message", MessageBoxButtons.OK);
                }                
                else
                {
                    //if (int.Parse(txtAmountRecite.Text.Trim(), out number))
                    //{
                    //    MessageBox.Show("Amount can be only numbers", "Error Message", MessageBoxButtons.OK);
                    //}
                    //else
                    //{

                   
                    //read and save information into database
                    var connectionStr = ConfigurationManager.ConnectionStrings["MyDb"].ConnectionString;
                    
                    //SqlConnection
                    using (var sqlConnection = new SqlConnection(connectionStr))
                    {
                        try
                        {
                            sqlConnection.Open();

                            String FirstName = txtCustFirstNameRecite.Text.ToString();
                            String LastName = txtCustLastNameRecite.Text.ToString();

                            //checking if search customer exits in your database
                            string qryCheck = "select CustFirstName from Customer where CustFirstName = '" + FirstName + "' and CustLastName = '" + LastName + "'";
                            SqlCommand cmdCheck = new SqlCommand(qryCheck, sqlConnection);
                            var reader = cmdCheck.ExecuteReader();
                            while (reader.Read())
                            {
                                var custFirstName = reader["CustFirstName"];
                                lblCustName.Text = custFirstName.ToString();
                            }

                            sqlConnection.Close();
                            
                            if (lblCustName.Text == "")
                            {
                                //message
                                MessageBoxButtons button = MessageBoxButtons.YesNo;
                                var result = MessageBox.Show("You don't have a customer on this name. Do you want to add new Customer?", "Warning Message", button);
                                if (result == DialogResult.Yes)
                                {
                                    new AddNewCustomer().Show();
                                }
                            }
                            else
                            {
                                // this.Hide(); 
                                sqlConnection.Open();
                                //reading ID from Customer table
                                string qryReadID = "select CustID from Customer  where CustFirstName = '" + FirstName + "' and CustLastName = '" + LastName + "'";
                                using (var cmd = new SqlCommand(qryReadID, sqlConnection))
                                {
                                    reader = cmd.ExecuteReader();
                                    while (reader.Read())
                                    {
                                        var custID = reader["CustID"];
                                        lblCustIDRecite.Text = custID.ToString();
                                    }
                                }
                                sqlConnection.Close();
                                sqlConnection.Open();
                                //reading Matricule Number from Company Table
                                string qryReadMatriculeNumber = "select MatriculeNumber from Company";
                                using (var cmd = new SqlCommand(qryReadMatriculeNumber, sqlConnection))
                                {

                                    reader = cmd.ExecuteReader();
                                    while (reader.Read())
                                    {
                                        var MatriculeNamber = reader["MatriculeNumber"];
                                        lblMatriculeNumber.Text = MatriculeNamber.ToString();

                                        //int value0 = Int16.Parse(custID.ToString());                           
                                    }
                                }
                                sqlConnection.Close();
                                sqlConnection.Open();

                                //Inserting information into Customer table
                                string qry = "insert into Recite( MatriculeNumber, CustID ,Amount, Date) values(@p1,@p2,@p3, @p4)";

                                using (SqlCommand cmd = new SqlCommand(qry, sqlConnection))
                                {

                                    cmd.Parameters.AddWithValue("@p1", lblMatriculeNumber.Text);
                                    cmd.Parameters.AddWithValue("@p2", Int32.Parse(lblCustIDRecite.Text.ToString()));
                                    cmd.Parameters.AddWithValue("@p3", txtAmountRecite.Text);
                                    cmd.Parameters.AddWithValue("@p4", dateTimePickerDateRecit.Text);

                                    int i = cmd.ExecuteNonQuery();
                                    if (i >= 1)
                                    {
                                        string message = i + " Recite was saved successfully";
                                        string caption = "Message";
                                        MessageBoxButtons button = MessageBoxButtons.OK;
                                        DialogResult result;

                                        // Displays the MessageBox.
                                        result = MessageBox.Show(message, caption, button);

                                        if (result == DialogResult.OK)
                                        {
                                            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                                            result = MessageBox.Show("Do you want to create the other Recite?", "Message", buttons);
                                            if (result == DialogResult.Yes)
                                            {
                                                this.Show();
                                                txtCustFirstNameRecite.Text = "";
                                                txtCustLastNameRecite.Text = "";
                                                txtAmountRecite.Text = "";
                                            }
                                            else
                                            {
                                                new Menu().Show();
                                                this.Hide();
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Customer is not Added", "Message");
                                        }
                                    }
                                }
                            }
                            sqlConnection.Close();
                        }
                        catch (System.Exception exep)
                        {
                            MessageBox.Show("Error is " + exep.ToString());
                        }

                    }
                }

            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Menu().Show();
        }        
    }
}

