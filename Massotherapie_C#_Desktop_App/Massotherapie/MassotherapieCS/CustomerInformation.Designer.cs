﻿namespace MassotherapieCS
{
    partial class CustomerInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCustomerFirstName = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.lblEmailAddress = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblCountry = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblZipCode = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCustomerLastName = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtCustFirstNameInfo = new System.Windows.Forms.TextBox();
            this.txtLastNameInfo = new System.Windows.Forms.TextBox();
            this.txtAddressInfo = new System.Windows.Forms.TextBox();
            this.txtZipCodeInfo = new System.Windows.Forms.TextBox();
            this.txtCityInfo = new System.Windows.Forms.TextBox();
            this.txtCountryInfo = new System.Windows.Forms.TextBox();
            this.txtPhoneInfo = new System.Windows.Forms.TextBox();
            this.txtEmailAddressInfo = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblCustIDInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCustomerFirstName
            // 
            this.lblCustomerFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerFirstName.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblCustomerFirstName.Location = new System.Drawing.Point(105, 37);
            this.lblCustomerFirstName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomerFirstName.Name = "lblCustomerFirstName";
            this.lblCustomerFirstName.Size = new System.Drawing.Size(142, 52);
            this.lblCustomerFirstName.TabIndex = 41;
            this.lblCustomerFirstName.Text = "First Name:";
            this.lblCustomerFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCancel
            // 
            this.btnCancel.AutoEllipsis = true;
            this.btnCancel.BackColor = System.Drawing.Color.PeachPuff;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(542, 533);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 62);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnModify
            // 
            this.btnModify.AutoEllipsis = true;
            this.btnModify.BackColor = System.Drawing.Color.SkyBlue;
            this.btnModify.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModify.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnModify.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnModify.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModify.Location = new System.Drawing.Point(198, 533);
            this.btnModify.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(134, 62);
            this.btnModify.TabIndex = 9;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = false;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailAddress.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblEmailAddress.Location = new System.Drawing.Point(105, 443);
            this.lblEmailAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Size = new System.Drawing.Size(196, 52);
            this.lblEmailAddress.TabIndex = 48;
            this.lblEmailAddress.Text = "Email Address:";
            this.lblEmailAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPhone
            // 
            this.lblPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblPhone.Location = new System.Drawing.Point(105, 385);
            this.lblPhone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(142, 52);
            this.lblPhone.TabIndex = 47;
            this.lblPhone.Text = "Phone:";
            this.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCountry
            // 
            this.lblCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountry.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblCountry.Location = new System.Drawing.Point(105, 327);
            this.lblCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(142, 52);
            this.lblCountry.TabIndex = 46;
            this.lblCountry.Text = "Country:";
            this.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCity
            // 
            this.lblCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCity.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblCity.Location = new System.Drawing.Point(105, 211);
            this.lblCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(142, 52);
            this.lblCity.TabIndex = 45;
            this.lblCity.Text = "City:";
            this.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZipCode
            // 
            this.lblZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZipCode.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblZipCode.Location = new System.Drawing.Point(105, 269);
            this.lblZipCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZipCode.Name = "lblZipCode";
            this.lblZipCode.Size = new System.Drawing.Size(142, 52);
            this.lblZipCode.TabIndex = 44;
            this.lblZipCode.Text = "Zip Code:";
            this.lblZipCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAddress
            // 
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblAddress.Location = new System.Drawing.Point(105, 153);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(142, 52);
            this.lblAddress.TabIndex = 43;
            this.lblAddress.Text = "Address:";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCustomerLastName
            // 
            this.lblCustomerLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerLastName.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblCustomerLastName.Location = new System.Drawing.Point(105, 95);
            this.lblCustomerLastName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomerLastName.Name = "lblCustomerLastName";
            this.lblCustomerLastName.Size = new System.Drawing.Size(142, 52);
            this.lblCustomerLastName.TabIndex = 42;
            this.lblCustomerLastName.Text = "Last Name:";
            this.lblCustomerLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDelete
            // 
            this.btnDelete.AutoEllipsis = true;
            this.btnDelete.BackColor = System.Drawing.Color.Tomato;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(370, 533);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(134, 62);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "Delete Customer";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtCustFirstNameInfo
            // 
            this.txtCustFirstNameInfo.BackColor = System.Drawing.Color.OldLace;
            this.txtCustFirstNameInfo.Enabled = false;
            this.txtCustFirstNameInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustFirstNameInfo.ForeColor = System.Drawing.Color.SeaGreen;
            this.txtCustFirstNameInfo.Location = new System.Drawing.Point(310, 48);
            this.txtCustFirstNameInfo.Multiline = true;
            this.txtCustFirstNameInfo.Name = "txtCustFirstNameInfo";
            this.txtCustFirstNameInfo.Size = new System.Drawing.Size(270, 40);
            this.txtCustFirstNameInfo.TabIndex = 1;
            // 
            // txtLastNameInfo
            // 
            this.txtLastNameInfo.BackColor = System.Drawing.Color.OldLace;
            this.txtLastNameInfo.Enabled = false;
            this.txtLastNameInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastNameInfo.ForeColor = System.Drawing.Color.SeaGreen;
            this.txtLastNameInfo.Location = new System.Drawing.Point(310, 106);
            this.txtLastNameInfo.Multiline = true;
            this.txtLastNameInfo.Name = "txtLastNameInfo";
            this.txtLastNameInfo.Size = new System.Drawing.Size(270, 40);
            this.txtLastNameInfo.TabIndex = 2;
            // 
            // txtAddressInfo
            // 
            this.txtAddressInfo.BackColor = System.Drawing.Color.OldLace;
            this.txtAddressInfo.Enabled = false;
            this.txtAddressInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddressInfo.ForeColor = System.Drawing.Color.SeaGreen;
            this.txtAddressInfo.Location = new System.Drawing.Point(310, 164);
            this.txtAddressInfo.Multiline = true;
            this.txtAddressInfo.Name = "txtAddressInfo";
            this.txtAddressInfo.Size = new System.Drawing.Size(270, 40);
            this.txtAddressInfo.TabIndex = 3;
            // 
            // txtZipCodeInfo
            // 
            this.txtZipCodeInfo.BackColor = System.Drawing.Color.OldLace;
            this.txtZipCodeInfo.Enabled = false;
            this.txtZipCodeInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtZipCodeInfo.ForeColor = System.Drawing.Color.SeaGreen;
            this.txtZipCodeInfo.Location = new System.Drawing.Point(310, 280);
            this.txtZipCodeInfo.Multiline = true;
            this.txtZipCodeInfo.Name = "txtZipCodeInfo";
            this.txtZipCodeInfo.Size = new System.Drawing.Size(270, 40);
            this.txtZipCodeInfo.TabIndex = 5;
            // 
            // txtCityInfo
            // 
            this.txtCityInfo.BackColor = System.Drawing.Color.OldLace;
            this.txtCityInfo.Enabled = false;
            this.txtCityInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCityInfo.ForeColor = System.Drawing.Color.SeaGreen;
            this.txtCityInfo.Location = new System.Drawing.Point(310, 222);
            this.txtCityInfo.Multiline = true;
            this.txtCityInfo.Name = "txtCityInfo";
            this.txtCityInfo.Size = new System.Drawing.Size(270, 40);
            this.txtCityInfo.TabIndex = 4;
            // 
            // txtCountryInfo
            // 
            this.txtCountryInfo.BackColor = System.Drawing.Color.OldLace;
            this.txtCountryInfo.Enabled = false;
            this.txtCountryInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryInfo.ForeColor = System.Drawing.Color.SeaGreen;
            this.txtCountryInfo.Location = new System.Drawing.Point(310, 338);
            this.txtCountryInfo.Multiline = true;
            this.txtCountryInfo.Name = "txtCountryInfo";
            this.txtCountryInfo.Size = new System.Drawing.Size(270, 40);
            this.txtCountryInfo.TabIndex = 6;
            // 
            // txtPhoneInfo
            // 
            this.txtPhoneInfo.BackColor = System.Drawing.Color.OldLace;
            this.txtPhoneInfo.Enabled = false;
            this.txtPhoneInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneInfo.ForeColor = System.Drawing.Color.SeaGreen;
            this.txtPhoneInfo.Location = new System.Drawing.Point(310, 396);
            this.txtPhoneInfo.Multiline = true;
            this.txtPhoneInfo.Name = "txtPhoneInfo";
            this.txtPhoneInfo.Size = new System.Drawing.Size(270, 40);
            this.txtPhoneInfo.TabIndex = 7;
            // 
            // txtEmailAddressInfo
            // 
            this.txtEmailAddressInfo.BackColor = System.Drawing.Color.OldLace;
            this.txtEmailAddressInfo.Enabled = false;
            this.txtEmailAddressInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailAddressInfo.ForeColor = System.Drawing.Color.SeaGreen;
            this.txtEmailAddressInfo.Location = new System.Drawing.Point(310, 454);
            this.txtEmailAddressInfo.Multiline = true;
            this.txtEmailAddressInfo.Name = "txtEmailAddressInfo";
            this.txtEmailAddressInfo.Size = new System.Drawing.Size(270, 40);
            this.txtEmailAddressInfo.TabIndex = 8;
            // 
            // btnSave
            // 
            this.btnSave.AutoEllipsis = true;
            this.btnSave.BackColor = System.Drawing.Color.CadetBlue;
            this.btnSave.Enabled = false;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(26, 533);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(134, 62);
            this.btnSave.TabIndex = 49;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblCustIDInfo
            // 
            this.lblCustIDInfo.AutoSize = true;
            this.lblCustIDInfo.Location = new System.Drawing.Point(12, 9);
            this.lblCustIDInfo.Name = "lblCustIDInfo";
            this.lblCustIDInfo.Size = new System.Drawing.Size(0, 20);
            this.lblCustIDInfo.TabIndex = 50;
            this.lblCustIDInfo.Visible = false;
            // 
            // CustomerInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OldLace;
            this.ClientSize = new System.Drawing.Size(708, 624);
            this.Controls.Add(this.lblCustIDInfo);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtEmailAddressInfo);
            this.Controls.Add(this.txtPhoneInfo);
            this.Controls.Add(this.txtCountryInfo);
            this.Controls.Add(this.txtCityInfo);
            this.Controls.Add(this.txtZipCodeInfo);
            this.Controls.Add(this.txtAddressInfo);
            this.Controls.Add(this.txtLastNameInfo);
            this.Controls.Add(this.txtCustFirstNameInfo);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lblCustomerFirstName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.lblEmailAddress);
            this.Controls.Add(this.lblPhone);
            this.Controls.Add(this.lblCountry);
            this.Controls.Add(this.lblCity);
            this.Controls.Add(this.lblZipCode);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lblCustomerLastName);
            this.Name = "CustomerInformation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Information";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblCustomerFirstName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Label lblEmailAddress;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblZipCode;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblCustomerLastName;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TextBox txtCustFirstNameInfo;
        private System.Windows.Forms.TextBox txtLastNameInfo;
        private System.Windows.Forms.TextBox txtAddressInfo;
        private System.Windows.Forms.TextBox txtZipCodeInfo;
        private System.Windows.Forms.TextBox txtCityInfo;
        private System.Windows.Forms.TextBox txtCountryInfo;
        private System.Windows.Forms.TextBox txtPhoneInfo;
        private System.Windows.Forms.TextBox txtEmailAddressInfo;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblCustIDInfo;
    }
}