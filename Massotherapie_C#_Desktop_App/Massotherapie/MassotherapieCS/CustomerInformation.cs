﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MassotherapieCS
{
    public partial class CustomerInformation : Form
    {
        

        public CustomerInformation(int CustId, String FirstName, String LastName, String Address, String City, String ZipCode, String Country, String Phone, String EmailAddress)
        {
            InitializeComponent();

            //Reading information from Find Customer Form
            lblCustIDInfo.Text = CustId.ToString();
            txtCustFirstNameInfo.Text = FirstName;
            txtLastNameInfo.Text = LastName;
            txtAddressInfo.Text = Address;
            txtCityInfo.Text = City;
            txtZipCodeInfo.Text = ZipCode;
            txtCountryInfo.Text = Country;
            txtPhoneInfo.Text = Phone;
            txtEmailAddressInfo.Text = EmailAddress;
        }


        private void btnModify_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            //Modifying textFields by activating the textboxes
            txtCustFirstNameInfo.Enabled = true;            
            txtLastNameInfo.Enabled = true;
            txtAddressInfo.Enabled = true;
            txtCityInfo.Enabled = true;
            txtZipCodeInfo.Enabled = true;            
            txtCountryInfo.Enabled = true;
            txtPhoneInfo.Enabled = true;
            txtEmailAddressInfo.Enabled = true;

            //changing textboxe's backgroung Color 
            Color FloralWhite = default(Color);
            txtCustFirstNameInfo.BackColor = FloralWhite;
            txtLastNameInfo.BackColor = FloralWhite;
            txtAddressInfo.BackColor = FloralWhite;
            txtCityInfo.BackColor = FloralWhite;
            txtZipCodeInfo.BackColor = FloralWhite;
            txtCountryInfo.BackColor = FloralWhite;
            txtPhoneInfo.BackColor = FloralWhite;
            txtEmailAddressInfo.BackColor = FloralWhite;
           
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            
            string message = "You Realy want to delete this Customer information?";
            string caption = "Message";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.
            result = MessageBox.Show(message, caption, buttons);

            if (result == DialogResult.Yes)
            {
                //delete information from database

                //save information in database
                var connectionStr = ConfigurationManager.ConnectionStrings["MyDb"].ConnectionString;

                //SqlConnection
                using (var sqlConnection = new SqlConnection(connectionStr))
                {
                    try
                    {
                        sqlConnection.Open();

                        //Inserting information into Customer table
                        int custID = Int32.Parse(lblCustIDInfo.Text);
                        string qry = "Delete from Customer WHERE CustId = '" + custID + "' ";

                        using (SqlCommand cmd = new SqlCommand(qry, sqlConnection))
                        {
                            
                            int i = cmd.ExecuteNonQuery();
                            if (i >= 1)
                            {
                                string firstName = txtCustFirstNameInfo.Text.ToString();
                                MessageBox.Show(firstName + "'s information was deleted successfully", "Message", MessageBoxButtons.OK);
                                new FindCustomer().Show();
                                this.Hide();
                            }
                        }
                    }
                    catch (System.Exception exep)
                    {
                        MessageBox.Show("Error is " + exep.ToString());
                    }
                }
                                
            }
            else
            {
                this.Show();
            }            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            string message = " Have you finish customer modification?";
            string caption = "Message";
            MessageBoxButtons button = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.
            result = MessageBox.Show(message, caption, button);

            if (result == DialogResult.Yes)
            {
                new Menu().Show();
                this.Hide();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            /////////////////
            //Validation part
            /////////////////

            //checking if required fields are empty or not
            if (txtCustFirstNameInfo.Text == "" || txtLastNameInfo.Text == "")
            {
                MessageBox.Show("You need to fullfill all requared field", "Error Message", MessageBoxButtons.OK);
            }
            else
            {
                //First Name and Last Name must be contain only letters
                if (!Regex.IsMatch(txtCustFirstNameInfo.Text, @"^[\p{L}]+$")
                    || !Regex.IsMatch(txtLastNameInfo.Text, @"^[\p{L}]+$"))
                {
                    MessageBox.Show("First Name and Last Name must be contain only letters", "Error Message", MessageBoxButtons.OK);
                }

                else
                {
                    // Email address must be valid one
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    if (txtEmailAddressInfo.Text != "" & !regex.IsMatch(txtEmailAddressInfo.Text))
                    {
                        MessageBox.Show("Please enter a valid email address", "Error Message", MessageBoxButtons.OK);
                    }

                    else
                    {
                        //Phone number must be in required format
                        if (txtPhoneInfo.Text != "" & !Regex.IsMatch(txtPhoneInfo.Text, "\\p{N}{3}-\\p{N}{3}-\\p{N}{4}\\b"))
                        {
                            MessageBox.Show("Phone number must be in this form 111-111-1111", "Error Message", MessageBoxButtons.OK);
                        }                        
                        else
                        {

                            //save information in database
                            var connectionStr = ConfigurationManager.ConnectionStrings["MyDb"].ConnectionString;


                            //SqlConnection
                            using (var sqlConnection = new SqlConnection(connectionStr))
                            {
                                try
                                {
                                    sqlConnection.Open();

                                    //Inserting information into Customer table
                                    int custID = Int32.Parse(lblCustIDInfo.Text);
                                    string qry = "UPDATE Customer SET CustfirstName = @p1, CustLastName = @p2, Address = @p3, City = @p4, ZipCode = @p5, Country = @p6, Phone = @p7, EmailAddress = @p8 WHERE CustId = '" + custID + "' ";
                                    //insert into Customer(CustfirstName, CustLastName, Address,City, ZipCode, Country, Phone, EmailAddress) values(@p1,@p2,@p3, @p4, @p5, @p6, @p7, @p8) where CustId = '"+custID+"' ";

                                    using (SqlCommand cmd = new SqlCommand(qry, sqlConnection))
                                    {
                                        cmd.Parameters.AddWithValue("@p1", txtCustFirstNameInfo.Text);
                                        cmd.Parameters.AddWithValue("@p2", txtLastNameInfo.Text);
                                        cmd.Parameters.AddWithValue("@p3", txtAddressInfo.Text);
                                        cmd.Parameters.AddWithValue("@p4", txtZipCodeInfo.Text);
                                        cmd.Parameters.AddWithValue("@p5", txtCityInfo.Text);
                                        cmd.Parameters.AddWithValue("@p6", txtCountryInfo.Text);
                                        cmd.Parameters.AddWithValue("@p7", txtPhoneInfo.Text);
                                        cmd.Parameters.AddWithValue("@p8", txtEmailAddressInfo.Text);

                                        int i = cmd.ExecuteNonQuery();
                                        if (i >= 1)
                                        {
                                            string firstName = txtCustFirstNameInfo.Text.ToString();
                                            string message = firstName + "'s information was modified and saved successfully";
                                            string caption = "Message";
                                            MessageBoxButtons button = MessageBoxButtons.OK;
                                            DialogResult result;

                                            // Displays the MessageBox.
                                            result = MessageBox.Show(message, caption, button);

                                            if (result == DialogResult.OK)
                                            {
                                                new FindCustomer().Show();
                                                this.Hide();

                                            }
                                        }
                                    }
                                }
                                catch (System.Exception exep)
                                {
                                    MessageBox.Show("Error is " + exep.ToString());
                                }
                            }
                        }

                    }
                }
            }
        }
        
    }
}
