﻿namespace MassotherapieCS
{
    partial class FindCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCustLastNameFind = new System.Windows.Forms.TextBox();
            this.lblCustLastNameFind = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCustNameFind = new System.Windows.Forms.TextBox();
            this.lblCustNameFind = new System.Windows.Forms.Label();
            this.lblCustName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtCustLastNameFind
            // 
            this.txtCustLastNameFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustLastNameFind.Location = new System.Drawing.Point(274, 132);
            this.txtCustLastNameFind.Multiline = true;
            this.txtCustLastNameFind.Name = "txtCustLastNameFind";
            this.txtCustLastNameFind.Size = new System.Drawing.Size(238, 47);
            this.txtCustLastNameFind.TabIndex = 2;
            // 
            // lblCustLastNameFind
            // 
            this.lblCustLastNameFind.AutoSize = true;
            this.lblCustLastNameFind.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustLastNameFind.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCustLastNameFind.Location = new System.Drawing.Point(41, 132);
            this.lblCustLastNameFind.Name = "lblCustLastNameFind";
            this.lblCustLastNameFind.Size = new System.Drawing.Size(223, 28);
            this.lblCustLastNameFind.TabIndex = 58;
            this.lblCustLastNameFind.Text = "Customer Last Name:";
            // 
            // btnCancel
            // 
            this.btnCancel.AutoEllipsis = true;
            this.btnCancel.BackColor = System.Drawing.Color.PeachPuff;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnCancel.Location = new System.Drawing.Point(367, 220);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(145, 61);
            this.btnCancel.TabIndex = 57;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.AutoEllipsis = true;
            this.btnSearch.BackColor = System.Drawing.Color.SteelBlue;
            this.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.BurlyWood;
            this.btnSearch.Location = new System.Drawing.Point(197, 220);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(145, 61);
            this.btnSearch.TabIndex = 56;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCustNameFind
            // 
            this.txtCustNameFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustNameFind.Location = new System.Drawing.Point(274, 62);
            this.txtCustNameFind.Multiline = true;
            this.txtCustNameFind.Name = "txtCustNameFind";
            this.txtCustNameFind.Size = new System.Drawing.Size(238, 47);
            this.txtCustNameFind.TabIndex = 1;
            // 
            // lblCustNameFind
            // 
            this.lblCustNameFind.AutoSize = true;
            this.lblCustNameFind.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustNameFind.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCustNameFind.Location = new System.Drawing.Point(41, 62);
            this.lblCustNameFind.Name = "lblCustNameFind";
            this.lblCustNameFind.Size = new System.Drawing.Size(227, 28);
            this.lblCustNameFind.TabIndex = 54;
            this.lblCustNameFind.Text = "Customer First Name:";
            // 
            // lblCustName
            // 
            this.lblCustName.AutoSize = true;
            this.lblCustName.Location = new System.Drawing.Point(12, 284);
            this.lblCustName.Name = "lblCustName";
            this.lblCustName.Size = new System.Drawing.Size(0, 20);
            this.lblCustName.TabIndex = 59;
            this.lblCustName.Visible = false;
            // 
            // FindCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OldLace;
            this.ClientSize = new System.Drawing.Size(583, 313);
            this.Controls.Add(this.lblCustName);
            this.Controls.Add(this.txtCustLastNameFind);
            this.Controls.Add(this.lblCustLastNameFind);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtCustNameFind);
            this.Controls.Add(this.lblCustNameFind);
            this.Name = "FindCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FindCustomer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCustLastNameFind;
        private System.Windows.Forms.Label lblCustLastNameFind;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtCustNameFind;
        private System.Windows.Forms.Label lblCustNameFind;
        private System.Windows.Forms.Label lblCustName;
    }
}