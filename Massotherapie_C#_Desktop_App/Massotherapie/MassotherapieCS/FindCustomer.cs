﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MassotherapieCS
{
    public partial class FindCustomer : Form
    {
        public FindCustomer()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            new Menu().Show();
            this.Hide();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {            
            //checking if First And Last name fields are empty or not
            if (txtCustNameFind.Text == "" || txtCustLastNameFind.Text == "")
            {
                MessageBox.Show("You need to fullfill First and Last name fields", "Error Message", MessageBoxButtons.OK);
            }
            else
            {
                //First Name and Last Name must be contain only letters
                if (!Regex.IsMatch(txtCustNameFind.Text, @"^[\p{L}]+$")
                    || !Regex.IsMatch(txtCustLastNameFind.Text, @"^[\p{L}]+$"))
                {
                    MessageBox.Show("First Name and Last Name must be contain only letters", "Error Message", MessageBoxButtons.OK);
                }
                else {
                    
                    //Customer Search in database
                    var connectionStr = ConfigurationManager.ConnectionStrings["MyDb"].ConnectionString;

                    //SqlConnection
                    using (var sqlConnection = new SqlConnection(connectionStr))
                    {
                        sqlConnection.Open();
                        String FirstName = txtCustNameFind.Text.ToString();
                        String LastName = txtCustLastNameFind.Text.ToString();

                        //checking if search customer exits in your database
                        string qryCheck = "select CustFirstName from Customer where CustFirstName = '" + FirstName + "' and CustLastName = '" + LastName + "'";
                        SqlCommand cmdCheck = new SqlCommand(qryCheck, sqlConnection);
                        var reader = cmdCheck.ExecuteReader();
                        while (reader.Read())
                        {
                            var custFirstName = reader["CustFirstName"];
                            lblCustName.Text = custFirstName.ToString();
                        }

                        sqlConnection.Close();

                        if (lblCustName.Text == "")
                        {
                            //message
                            MessageBoxButtons button = MessageBoxButtons.OK;
                            var result = MessageBox.Show("You don't have a customer in this name.", "Warning Message", button);
                        }
                        else
                        {
                            this.Hide();
                            sqlConnection.Open();
                            //SqlCommand
                            var cmdStr = "select * from [Customer] where CustFirstName = '" + FirstName + "' And CustLastName = '" + LastName + "' ";
                            using (var cmd = new SqlCommand(cmdStr, sqlConnection))
                            {
                                reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    var custID = reader["CustID"];
                                    var custFirstName = reader["CustFirstName"];
                                    var custLastName = reader["CustLastName"];
                                    var Address = reader["Address"];
                                    var City = reader["City"];
                                    var ZipCode = reader["ZipCode"];
                                    var Country = reader["Country"];
                                    var Phone = reader["Phone"];
                                    var EmailAddress = reader["EmailAddress"];


                                    //Passing parameter from one Form to the other
                                    int value0 = Int16.Parse(custID.ToString());
                                    String value1 = custFirstName.ToString();
                                    String value2 = custLastName.ToString();
                                    String value3 = Address.ToString();
                                    String value4 = City.ToString();
                                    String value5 = ZipCode.ToString();
                                    String value6 = Country.ToString();
                                    String value7 = Phone.ToString();
                                    String value8 = EmailAddress.ToString();
                                    CustomerInformation form2 = new CustomerInformation(value0, value1, value2, value3, value4, value5, value6, value7, value8);
                                    form2.Show();
                                }
                            }
                            sqlConnection.Close();
                        }

                    }//End using   
                }              
            }
        }//End button Search  
    }
}
