﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MassotherapieCS
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void btnNewCustomer_Click(object sender, EventArgs e)
        {
            new AddNewCustomer().Show();
            this.Hide();
        }
       
        private void btnMakeReport_Click(object sender, EventArgs e)
        {
            new btnCreateReport().Show();
            this.Hide();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFindCustomer_Click(object sender, EventArgs e)
        {
            new FindCustomer().Show();
            this.Hide();
        }

        private void Recipte_Click(object sender, EventArgs e)
        {
            new CreateRecite().Show();
            this.Hide();
        }
    }
}
