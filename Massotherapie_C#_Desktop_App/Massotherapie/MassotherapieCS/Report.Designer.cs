﻿namespace MassotherapieCS
{
    partial class btnCreateReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(btnCreateReport));
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.lblMonthReport = new System.Windows.Forms.Label();
            this.dtpDateReport = new System.Windows.Forms.DateTimePicker();
            this.lblDateReport = new System.Windows.Forms.Label();
            this.lblCustFirstNameReport = new System.Windows.Forms.Label();
            this.txtCustName = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtCustLastName = new System.Windows.Forms.TextBox();
            this.lblCustLastNameReport = new System.Windows.Forms.Label();
            this.printDocumentForRecipts = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbYearMonthCustName = new System.Windows.Forms.RadioButton();
            this.rbYearCustName = new System.Windows.Forms.RadioButton();
            this.rbCustName = new System.Windows.Forms.RadioButton();
            this.rbDateCustName = new System.Windows.Forms.RadioButton();
            this.rbDate = new System.Windows.Forms.RadioButton();
            this.rbYearMonth = new System.Windows.Forms.RadioButton();
            this.rbYear = new System.Windows.Forms.RadioButton();
            this.cmbYear = new System.Windows.Forms.ComboBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblCustName = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbMonth
            // 
            this.cmbMonth.Enabled = false;
            this.cmbMonth.ForeColor = System.Drawing.Color.DimGray;
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Items.AddRange(new object[] {
            "",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cmbMonth.Location = new System.Drawing.Point(635, 84);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(131, 28);
            this.cmbMonth.TabIndex = 2;
            // 
            // lblMonthReport
            // 
            this.lblMonthReport.AutoSize = true;
            this.lblMonthReport.Font = new System.Drawing.Font("Segoe UI Black", 10F);
            this.lblMonthReport.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblMonthReport.Location = new System.Drawing.Point(526, 84);
            this.lblMonthReport.Name = "lblMonthReport";
            this.lblMonthReport.Size = new System.Drawing.Size(84, 28);
            this.lblMonthReport.TabIndex = 2;
            this.lblMonthReport.Text = "Month:";
            // 
            // dtpDateReport
            // 
            this.dtpDateReport.CalendarForeColor = System.Drawing.Color.SteelBlue;
            this.dtpDateReport.CalendarTitleForeColor = System.Drawing.Color.SaddleBrown;
            this.dtpDateReport.CalendarTrailingForeColor = System.Drawing.Color.SteelBlue;
            this.dtpDateReport.Enabled = false;
            this.dtpDateReport.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateReport.Location = new System.Drawing.Point(635, 126);
            this.dtpDateReport.Name = "dtpDateReport";
            this.dtpDateReport.Size = new System.Drawing.Size(174, 26);
            this.dtpDateReport.TabIndex = 3;
            this.dtpDateReport.Value = new System.DateTime(2018, 5, 8, 16, 47, 10, 0);
            // 
            // lblDateReport
            // 
            this.lblDateReport.AutoSize = true;
            this.lblDateReport.Font = new System.Drawing.Font("Segoe UI Black", 10F);
            this.lblDateReport.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblDateReport.Location = new System.Drawing.Point(547, 126);
            this.lblDateReport.Name = "lblDateReport";
            this.lblDateReport.Size = new System.Drawing.Size(63, 28);
            this.lblDateReport.TabIndex = 0;
            this.lblDateReport.Text = "Date:";
            // 
            // lblCustFirstNameReport
            // 
            this.lblCustFirstNameReport.AutoSize = true;
            this.lblCustFirstNameReport.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustFirstNameReport.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCustFirstNameReport.Location = new System.Drawing.Point(485, 175);
            this.lblCustFirstNameReport.Name = "lblCustFirstNameReport";
            this.lblCustFirstNameReport.Size = new System.Drawing.Size(125, 28);
            this.lblCustFirstNameReport.TabIndex = 4;
            this.lblCustFirstNameReport.Text = "First Name:";
            // 
            // txtCustName
            // 
            this.txtCustName.Enabled = false;
            this.txtCustName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.txtCustName.Location = new System.Drawing.Point(635, 175);
            this.txtCustName.Multiline = true;
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new System.Drawing.Size(238, 47);
            this.txtCustName.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.AutoEllipsis = true;
            this.btnCancel.BackColor = System.Drawing.Color.PeachPuff;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnCancel.Location = new System.Drawing.Point(552, 366);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(145, 61);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtCustLastName
            // 
            this.txtCustLastName.Enabled = false;
            this.txtCustLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustLastName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.txtCustLastName.Location = new System.Drawing.Point(635, 236);
            this.txtCustLastName.Multiline = true;
            this.txtCustLastName.Name = "txtCustLastName";
            this.txtCustLastName.Size = new System.Drawing.Size(238, 47);
            this.txtCustLastName.TabIndex = 6;
            // 
            // lblCustLastNameReport
            // 
            this.lblCustLastNameReport.AutoSize = true;
            this.lblCustLastNameReport.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustLastNameReport.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCustLastNameReport.Location = new System.Drawing.Point(489, 236);
            this.lblCustLastNameReport.Name = "lblCustLastNameReport";
            this.lblCustLastNameReport.Size = new System.Drawing.Size(121, 28);
            this.lblCustLastNameReport.TabIndex = 52;
            this.lblCustLastNameReport.Text = "Last Name:";
            // 
            // printDocumentForRecipts
            // 
            this.printDocumentForRecipts.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocumentForRecipts_PrintPage);
            // 
            // printPreviewDialog
            // 
            this.printPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog.Enabled = true;
            this.printPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog.Icon")));
            this.printPreviewDialog.Name = "printPreviewDialog";
            this.printPreviewDialog.Visible = false;
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.AutoEllipsis = true;
            this.btnPrintPreview.BackColor = System.Drawing.Color.SteelBlue;
            this.btnPrintPreview.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnPrintPreview.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnPrintPreview.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnPrintPreview.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintPreview.ForeColor = System.Drawing.Color.BurlyWood;
            this.btnPrintPreview.Location = new System.Drawing.Point(376, 366);
            this.btnPrintPreview.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Size = new System.Drawing.Size(145, 61);
            this.btnPrintPreview.TabIndex = 6;
            this.btnPrintPreview.Text = "Print Preview";
            this.btnPrintPreview.UseVisualStyleBackColor = false;
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbYearMonthCustName);
            this.groupBox1.Controls.Add(this.rbYearCustName);
            this.groupBox1.Controls.Add(this.rbCustName);
            this.groupBox1.Controls.Add(this.rbDateCustName);
            this.groupBox1.Controls.Add(this.rbDate);
            this.groupBox1.Controls.Add(this.rbYearMonth);
            this.groupBox1.Controls.Add(this.rbYear);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Black", 10F);
            this.groupBox1.ForeColor = System.Drawing.Color.SteelBlue;
            this.groupBox1.Location = new System.Drawing.Point(26, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 277);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Create Report ";
            // 
            // rbYearMonthCustName
            // 
            this.rbYearMonthCustName.AutoSize = true;
            this.rbYearMonthCustName.Location = new System.Drawing.Point(6, 197);
            this.rbYearMonthCustName.Name = "rbYearMonthCustName";
            this.rbYearMonthCustName.Size = new System.Drawing.Size(404, 32);
            this.rbYearMonthCustName.TabIndex = 0;
            this.rbYearMonthCustName.Text = "By Year, Month  and Customer Name";
            this.rbYearMonthCustName.UseVisualStyleBackColor = true;
            this.rbYearMonthCustName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rbYearMonthCustName_MouseDown);
            // 
            // rbYearCustName
            // 
            this.rbYearCustName.AutoSize = true;
            this.rbYearCustName.Location = new System.Drawing.Point(6, 165);
            this.rbYearCustName.Name = "rbYearCustName";
            this.rbYearCustName.Size = new System.Drawing.Size(320, 32);
            this.rbYearCustName.TabIndex = 0;
            this.rbYearCustName.Text = "By Year and Customer Name";
            this.rbYearCustName.UseVisualStyleBackColor = true;
            this.rbYearCustName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rbYearCustName_MouseDown);
            // 
            // rbCustName
            // 
            this.rbCustName.AutoSize = true;
            this.rbCustName.Location = new System.Drawing.Point(6, 133);
            this.rbCustName.Name = "rbCustName";
            this.rbCustName.Size = new System.Drawing.Size(227, 32);
            this.rbCustName.TabIndex = 0;
            this.rbCustName.Text = "By Customer Name";
            this.rbCustName.UseVisualStyleBackColor = true;
            this.rbCustName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rbCustName_MouseDown);
            // 
            // rbDateCustName
            // 
            this.rbDateCustName.AutoSize = true;
            this.rbDateCustName.Location = new System.Drawing.Point(6, 235);
            this.rbDateCustName.Name = "rbDateCustName";
            this.rbDateCustName.Size = new System.Drawing.Size(404, 32);
            this.rbDateCustName.TabIndex = 0;
            this.rbDateCustName.TabStop = true;
            this.rbDateCustName.Text = "By Spesify Date  and Customer Name";
            this.rbDateCustName.UseVisualStyleBackColor = true;
            this.rbDateCustName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rbDateCustName_MouseDown);
            // 
            // rbDate
            // 
            this.rbDate.AutoSize = true;
            this.rbDate.Location = new System.Drawing.Point(6, 101);
            this.rbDate.Name = "rbDate";
            this.rbDate.Size = new System.Drawing.Size(190, 32);
            this.rbDate.TabIndex = 0;
            this.rbDate.Text = "By Spesify Date";
            this.rbDate.UseVisualStyleBackColor = true;
            this.rbDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rbDate_MouseDown);
            // 
            // rbYearMonth
            // 
            this.rbYearMonth.AutoSize = true;
            this.rbYearMonth.Location = new System.Drawing.Point(6, 69);
            this.rbYearMonth.Name = "rbYearMonth";
            this.rbYearMonth.Size = new System.Drawing.Size(227, 32);
            this.rbYearMonth.TabIndex = 0;
            this.rbYearMonth.Text = "By Year and Month";
            this.rbYearMonth.UseVisualStyleBackColor = true;
            this.rbYearMonth.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rbYearMonth_MouseDown);
            // 
            // rbYear
            // 
            this.rbYear.AutoSize = true;
            this.rbYear.Checked = true;
            this.rbYear.Location = new System.Drawing.Point(6, 37);
            this.rbYear.Name = "rbYear";
            this.rbYear.Size = new System.Drawing.Size(112, 32);
            this.rbYear.TabIndex = 0;
            this.rbYear.TabStop = true;
            this.rbYear.Text = "By Year";
            this.rbYear.UseVisualStyleBackColor = true;
            this.rbYear.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rbYear_MouseDown);
            // 
            // cmbYear
            // 
            this.cmbYear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbYear.ForeColor = System.Drawing.Color.SaddleBrown;
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.Items.AddRange(new object[] {
            "",
            "2018",
            "2017",
            "2016",
            "2015",
            "2014",
            "2013",
            "2012",
            "2011",
            "2010",
            "2009"});
            this.cmbYear.Location = new System.Drawing.Point(635, 43);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(131, 28);
            this.cmbYear.TabIndex = 1;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Font = new System.Drawing.Font("Segoe UI Black", 10F);
            this.lblYear.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblYear.Location = new System.Drawing.Point(548, 43);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(62, 28);
            this.lblYear.TabIndex = 54;
            this.lblYear.Text = "Year:";
            // 
            // lblCustName
            // 
            this.lblCustName.AutoSize = true;
            this.lblCustName.Location = new System.Drawing.Point(28, 407);
            this.lblCustName.Name = "lblCustName";
            this.lblCustName.Size = new System.Drawing.Size(0, 20);
            this.lblCustName.TabIndex = 55;
            this.lblCustName.Visible = false;
            // 
            // btnCreateReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OldLace;
            this.ClientSize = new System.Drawing.Size(918, 461);
            this.Controls.Add(this.lblCustName);
            this.Controls.Add(this.dtpDateReport);
            this.Controls.Add(this.cmbMonth);
            this.Controls.Add(this.lblDateReport);
            this.Controls.Add(this.cmbYear);
            this.Controls.Add(this.lblMonthReport);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtCustLastName);
            this.Controls.Add(this.lblCustLastNameReport);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPrintPreview);
            this.Controls.Add(this.txtCustName);
            this.Controls.Add(this.lblCustFirstNameReport);
            this.Name = "btnCreateReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cmbMonth;
        private System.Windows.Forms.Label lblMonthReport;
        private System.Windows.Forms.DateTimePicker dtpDateReport;
        private System.Windows.Forms.Label lblDateReport;
        private System.Windows.Forms.Label lblCustFirstNameReport;
        private System.Windows.Forms.TextBox txtCustName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtCustLastName;
        private System.Windows.Forms.Label lblCustLastNameReport;
        private System.Drawing.Printing.PrintDocument printDocumentForRecipts;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog;
        private System.Windows.Forms.Button btnPrintPreview;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbYearMonthCustName;
        private System.Windows.Forms.RadioButton rbYearCustName;
        private System.Windows.Forms.RadioButton rbCustName;
        private System.Windows.Forms.RadioButton rbDateCustName;
        private System.Windows.Forms.RadioButton rbDate;
        private System.Windows.Forms.RadioButton rbYearMonth;
        private System.Windows.Forms.RadioButton rbYear;
        private System.Windows.Forms.ComboBox cmbYear;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label lblCustName;
    }
}