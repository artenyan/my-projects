﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MassotherapieCS
{
    public partial class btnCreateReport : Form
    {
        public btnCreateReport()
        {
            InitializeComponent();         
        }
        
        public static void PrintLine(int n, SqlCommand sqlCommand, System.Drawing.Printing.PrintPageEventArgs e)
        {
            var reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                //Reading table information into new variables
                var ReciteNumber = reader["ReciteNumber"];
                var CustFirstName = reader["CustFirstName"];
                var CustLastName = reader["CustLastName"];
                var Amount = reader["Amount"];
                var Date = reader["Date"];                
               
                //Designing each line for print
                e.Graphics.DrawString(""+ReciteNumber, new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 0, 150+n);
                e.Graphics.DrawString(""+CustFirstName, new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 180, 150+n);
                e.Graphics.DrawString(""+CustLastName, new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 360, 150+n);
                e.Graphics.DrawString(""+Amount, new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 540, 150+n);
                e.Graphics.DrawString(""+Date, new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 720, 150+n);
                e.Graphics.DrawLine(new Pen(Color.Black, 3), 0, 177+n, 830, 177+n);
                n += 30;
            }
        }       

        private void btnCancel_Click(object sender, EventArgs e)
        {
            var message = "Are you finish your job with creating report?";
            string caption = "Message";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.
            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                new Menu().Show();
                this.Close();
            }
        }

        private void btnPrintPreview_Click(object sender, EventArgs e)
        {
            //if ((rbYear.Checked & cmbYear.Text != "")
            //    || (rbYearCustName.Checked & cmbYear.Text != "" & txtCustLastName.Text != "" & txtCustName.Text != "")
            //    || (rbYearMonth.Checked & cmbYear.Text != "" & cmbMonth.Text != "")
            //    || (rbYearMonthCustName.Checked & cmbYear.Text != "" & cmbMonth.Text != "" & txtCustLastName.Text != "" & txtCustName.Text != "")
            //    || (rbDateCustName.Checked & txtCustLastName.Text != "" & txtCustName.Text != "")
            //    || (rbCustName.Checked & txtCustLastName.Text != "" & txtCustName.Text != ""))
            if ((rbYear.Checked & cmbYear.Text == "")
                || (rbYearCustName.Checked & cmbYear.Text == "" & txtCustLastName.Text == "" & txtCustName.Text == "")
                || (rbYearMonth.Checked & cmbYear.Text == "" & cmbMonth.Text == "")
                || (rbYearMonthCustName.Checked & cmbYear.Text == "" & cmbMonth.Text == "" & txtCustLastName.Text == "" & txtCustName.Text == "")
                || (rbDateCustName.Checked & txtCustLastName.Text == "" & txtCustName.Text == "")
                || (rbCustName.Checked & txtCustLastName.Text == "" & txtCustName.Text == ""))

            {
                MessageBox.Show("Please fill all active text boxes before Print Preview");
            }
            //First Name and Last Name must be contain only letters
            else if (txtCustName.Enabled == true & !Regex.IsMatch(txtCustName.Text, @"^[\p{L}]+$"))
            {
                MessageBox.Show("First Name must be contain only letters", "Error Message", MessageBoxButtons.OK);
            }
            else if (txtCustLastName.Enabled == true & !Regex.IsMatch(txtCustLastName.Text, @"^[\p{L}]+$"))
            {
                MessageBox.Show("Last Name must be contain only letters", "Error Message", MessageBoxButtons.OK);
            }
            else if (txtCustLastName.Text != "" & txtCustName.Text != "")
            {
                //Customer Search in database
                var connectionStr = ConfigurationManager.ConnectionStrings["MyDb"].ConnectionString;
            
                //SqlConnection
                using (var sqlConnection = new SqlConnection(connectionStr))
                {
                    sqlConnection.Open();
                    String FirstName = txtCustName.Text.ToString();
                    String LastName = txtCustLastName.Text.ToString();

                    //checking if search customer exits in your database
                    string qryCheck = "select CustFirstName from Customer where CustFirstName = '" + FirstName + "' and CustLastName = '" + LastName + "'";
                    SqlCommand cmdCheck = new SqlCommand(qryCheck, sqlConnection);
                    var reader = cmdCheck.ExecuteReader();
                    while (reader.Read())
                    {
                        var custFirstName = reader["CustFirstName"];
                        lblCustName.Text = custFirstName.ToString();
                    }

                    sqlConnection.Close();

                    if (lblCustName.Text != "")
                    {
                       // MessageBox.Show("I am in print document");
                        printPreviewDialog.Document = printDocumentForRecipts;
                        printPreviewDialog.ShowDialog();
                    }
                    else
                    {
                        //message
                        MessageBoxButtons button = MessageBoxButtons.YesNo;
                        DialogResult result = MessageBox.Show("You don't have a customer in this name. Do you want add as a new customer?", "Warning Message", button);
                        if (result == DialogResult.Yes)
                        {
                            new AddNewCustomer().Show();
                            this.Hide();
                        }
                    }
                }//end using
            }
            else
            {
                printPreviewDialog.Document = printDocumentForRecipts;
                printPreviewDialog.ShowDialog();
            }

        }
        private void printDocumentForRecipts_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //MessageBox.Show("I am in Print check");
            //Inserting Picture for logo
            Bitmap bmp = Properties.Resources.LogoMassotherapie0;
            Image newImage = bmp;
            e.Graphics.DrawImage(newImage, 310, 25, 240, 100);

            //Drawing first Line
            e.Graphics.DrawString("Recite Number ", new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 0, 150);
            e.Graphics.DrawString("First Name ", new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 180, 150);
            e.Graphics.DrawString("Last Name ", new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 360, 150);
            e.Graphics.DrawString("Amount ", new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 540, 150);
            e.Graphics.DrawString("Date ", new Font("Arial", 16, FontStyle.Regular), Brushes.Black, 720, 150);

            //////////////////
            //Search in database
            var connectionStr = ConfigurationManager.ConnectionStrings["MyDb"].ConnectionString;

            //SqlConnection
            using (var sqlConnection = new SqlConnection(connectionStr))
            {
                sqlConnection.Open();
                if (rbYear.Checked)
                {                   
                    var sqlCmd = new SqlCommand("ReportByYear", sqlConnection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.AddWithValue("@Year", SqlDbType.Int).Value = Int16.Parse(cmbYear.Text.Trim());
                    PrintLine(30, sqlCmd, e);
                }
                else if (rbYearMonth.Checked)
                {
                    var sqlCmd = new SqlCommand("ReportByYearMonth", sqlConnection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.AddWithValue("@Year", SqlDbType.Int).Value = Int16.Parse(cmbYear.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Month", SqlDbType.Int).Value = Int16.Parse(cmbMonth.Text.Trim());
                    PrintLine(30, sqlCmd, e);
                }
                else if (rbYearCustName.Checked)
                {
                    var sqlCmd = new SqlCommand("ReportByYearCustName", sqlConnection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.AddWithValue("@Year", SqlDbType.Int).Value = Int16.Parse(cmbYear.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@CustFirstName", SqlDbType.NVarChar).Value = (txtCustName.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@CustLastName", SqlDbType.NVarChar).Value = (txtCustLastName.Text.Trim());
                    PrintLine(30, sqlCmd, e);
                }
                else if (rbYearMonthCustName.Checked)
                {

                    var sqlCmd = new SqlCommand("ReportByYearMonthCustName", sqlConnection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.AddWithValue("@Year", SqlDbType.Int).Value = Int16.Parse(cmbYear.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Month", SqlDbType.Int).Value = Int16.Parse(cmbMonth.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@CustFirstName", SqlDbType.NVarChar).Value = (txtCustName.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@CustLastName", SqlDbType.NVarChar).Value = (txtCustLastName.Text.Trim());
                    PrintLine(30, sqlCmd, e);
                }
                else if (rbDate.Checked)
                {
                    var sqlCmd = new SqlCommand("ReportByDate", sqlConnection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.AddWithValue("@Date", SqlDbType.DateTime).Value = dtpDateReport.Value;

                    PrintLine(30, sqlCmd, e);
                }
                else if (rbDateCustName.Checked)
                {
                    var sqlCmd = new SqlCommand("ReportByDateCustName", sqlConnection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.AddWithValue("@Date", SqlDbType.DateTime).Value = dtpDateReport.Value;
                    sqlCmd.Parameters.AddWithValue("@CustFirstName", SqlDbType.NVarChar).Value = (txtCustName.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@CustLastName", SqlDbType.NVarChar).Value = (txtCustLastName.Text.Trim());

                    PrintLine(30, sqlCmd, e);
                }
                else if (rbCustName.Checked)
                {
                    var sqlCmd = new SqlCommand("ReportByCustName", sqlConnection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.AddWithValue("@CustFirstName", SqlDbType.NVarChar).Value = (txtCustName.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@CustLastName", SqlDbType.NVarChar).Value = (txtCustLastName.Text.Trim());

                    PrintLine(30, sqlCmd, e);
                }

                sqlConnection.Close();

            }//End using              
        }

        //Radio buttons events
        #region
        private void rbYear_MouseDown(object sender, MouseEventArgs e)
        {
            cmbMonth.Text = "";
            txtCustName.Text = "";
            txtCustLastName.Text = "";

            cmbYear.Enabled = true;
            cmbMonth.Enabled = false;
            dtpDateReport.Enabled = false;
            txtCustName.Enabled = false;
            txtCustLastName.Enabled = false;
        }

        private void rbYearMonth_MouseDown(object sender, MouseEventArgs e)
        {            
            txtCustName.Text = "";
            txtCustLastName.Text = "";

            cmbYear.Enabled = true;
            cmbMonth.Enabled = true;
            dtpDateReport.Enabled = false;
            txtCustName.Enabled = false;
            txtCustLastName.Enabled = false;
        }

        private void rbDate_MouseDown(object sender, MouseEventArgs e)
        {
            cmbYear.Text = "";
            cmbMonth.Text = "";
            txtCustName.Text = "";
            txtCustLastName.Text = "";

            cmbYear.Enabled = false;
            cmbMonth.Enabled = false;
            dtpDateReport.Enabled = true;
            txtCustName.Enabled = false;
            txtCustLastName.Enabled = false;
        }

        private void rbCustName_MouseDown(object sender, MouseEventArgs e)
        {
            //Clean not needed fields
            cmbYear.Text = "";
            cmbMonth.Text = "";

            //Made needed fields active
            cmbYear.Enabled = false;
            cmbMonth.Enabled = false;
            dtpDateReport.Enabled = false;
            txtCustName.Enabled = true;
            txtCustLastName.Enabled = true;
        }

        private void rbYearCustName_MouseDown(object sender, MouseEventArgs e)
        {
            cmbMonth.Text = "";

            cmbYear.Enabled = true;
            cmbMonth.Enabled = false;
            dtpDateReport.Enabled = false;
            txtCustName.Enabled = true;
            txtCustLastName.Enabled = true;
        }

        private void rbYearMonthCustName_MouseDown(object sender, MouseEventArgs e)
        {
            cmbYear.Enabled = true;
            cmbMonth.Enabled = true;
            dtpDateReport.Enabled = false;
            txtCustName.Enabled = true;
            txtCustLastName.Enabled = true;
        }

        private void rbDateCustName_MouseDown(object sender, MouseEventArgs e)
        {
            cmbYear.Text = "";
            cmbMonth.Text = "";

            cmbYear.Enabled = false;
            cmbMonth.Enabled = false;
            dtpDateReport.Enabled = true;
            txtCustName.Enabled = true;
            txtCustLastName.Enabled = true;
        }

        #endregion

        
    }
}
