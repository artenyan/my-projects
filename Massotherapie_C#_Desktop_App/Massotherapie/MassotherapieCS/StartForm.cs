﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MassotherapieCS
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
            this.timerCurrent.Enabled = true;
            this.timerCurrent.Interval = 3500;
        }

        private void timerCurrent_Tick(object sender, EventArgs e)
        {
            timerCurrent.Stop();
            new Menu().Show();
            this.Hide();
        }
    }
}
