﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SunnyRent.Models;
using System.Data.Entity;
using SunnyRent.ViewModel;

namespace SunnyRent.Controllers
{
    public class HousesController : Controller
    {
        private ApplicationDbContext _context;

        public HousesController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Houses
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Cottages()
        {
            string viewName = "CustomerView";

            if (User.IsInRole(RoleNames.CanManageMedia))
                viewName = "AdminView";
            var house = _context.Houses.Include(h => h.Image).ToList();
            
            return View(viewName,house);
        }

        public ActionResult Details(int? id)
        {
            string viewName = "CustomerDetailsView";

            if (User.IsInRole(RoleNames.CanManageMedia))
                viewName = "AdminDetailsView";

            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            //var house = _context.Houses.Include(h => h.HouseType).SingleOrDefault(h => h.Id == id);
           // var viewModel = _context.Reservations.Include(r => r.House).Where(r => r.HouseId == id).ToList();

            
            var viewModel = new HouseReservationViewModel
            {
                House = _context.Houses.Include(h => h.HouseType).Include(h => h.Image).SingleOrDefault(h => h.Id == id),
                Reservations = _context.Reservations.Include(r => r.House).Where(r => r.HouseId == id).ToList()
            };
            

            if (viewModel == null)
            {
                return HttpNotFound();
            }

            return View(viewName,viewModel);
        }
       
        //This action will display a confirm message to the user to cinfirm the delete operation
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
                return HttpNotFound();

            var house = _context.Houses.Include(h => h.HouseType).SingleOrDefault(h => h.Id == id);

            if (house == null)
                return HttpNotFound();

            return View(house);
        }
        [HttpPost]
        public ActionResult Delete(int id)
        {
            var house = _context.Houses.Find(id);
            var images = _context.Images.Where(i => i.HouseId == id).ToList();
            foreach(Image image in images)
            {
                _context.Images.Remove(image);
            }
            _context.Houses.Remove(house);
            _context.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        //Edit customer details
        public ActionResult Edit(int id)
        {
            var houseInDB = _context.Houses.SingleOrDefault(h => h.Id == id);

            //if (houseInDB == null)
            //    return HttpNotFound();

            var viewModel = new HouseFormViewModel()
            {
                House = houseInDB,
                HouseTypes = _context.HouseTypes.ToList()
            };

            return View("HouseForm", viewModel);
        }

        //Get action to load the form (1)
        public ActionResult New()
        {
            var viewModel = new HouseFormViewModel
            {
                House = new House(),
                HouseTypes = _context.HouseTypes.ToList()
            };
            return View("HouseForm", viewModel);
        }

        //Post action to save data from my form (2)            
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(House house)
        {
            //Check if the form is valid
            if (!ModelState.IsValid)
            {
                //Return the same form back to the user
                var viewModel = new HouseFormViewModel
                {
                    House = house,
                    HouseTypes = _context.HouseTypes.ToList()
                };
                return View("HouseForm", viewModel);
            }

            //new house
            if (house.Id == 0)
            {
                _context.Houses.Add(house);
            }
            else
            {
                var houseInDB = _context.Houses.Single(h => h.Id == house.Id);

                //Manual update
                houseInDB.Rate = house.Rate;
                houseInDB.Name = house.Name;
                houseInDB.Description = house.Description;
                houseInDB.Address = house.Address;
                houseInDB.TV = house.TV;
                houseInDB.Pet = house.Pet;
                houseInDB.BBQ = house.BBQ;
                houseInDB.FirePlace = house.FirePlace;
                houseInDB.AccessToWater = house.AccessToWater;
                houseInDB.Pool = house.Pool;
                houseInDB.Smoking = house.Smoking;
                houseInDB.WiFi = house.WiFi;
                houseInDB.HouseType = house.HouseType;

                //imageInDB.ImageName = image.ImageName;
                //imageInDB.ImagePath = image.ImagePath;
                //TryUpdateModel(houseInDB);
            }

            _context.SaveChanges();
            return RedirectToAction("Cottages", "Houses");
        }

        [HttpPost]
        public ActionResult Upload(IEnumerable<HttpPostedFileBase> uploads)
        {
            foreach (var file in uploads)
            {
                if (file != null)
                {
                    // we get the name of file
                    string fileName = System.IO.Path.GetFileName(file.FileName);
                    // save file into folder of our project
                    file.SaveAs(Server.MapPath("~/Photoes/Uploaded/" + fileName));
                }
            }

            return RedirectToAction("HouseForm", "Houses");
        }

        [HttpPost]
        public ActionResult HouseForm(IEnumerable<HttpPostedFileBase> uploads, House house)
        {
            List<Image> imageList = new List<Image>();

            foreach (var file in uploads)
            {
                if (file != null)
                {
                    // we get the name of file
                    string fileName = System.IO.Path.GetFileName(file.FileName);
                    // save file into folder of our project
                    file.SaveAs(Server.MapPath("~/Photoes/Uploaded/" + fileName));
                    Image Image = new Image
                    {
                        ImageName = System.IO.Path.GetFileName(file.FileName),
                        ImagePath = "~/Photoes/Uploaded/" + fileName,
                        HouseId = house.Id
                    };
                    _context.Images.Add(Image);
                    _context.SaveChanges();

                    imageList.Add(Image);
                }

            }

            var viewModel = new HouseFormViewModel
            {
                House = house,
                HouseTypes = _context.HouseTypes.ToList(),
               // ImageList = imageList
            };



            return View(viewModel);
        }


    }
}