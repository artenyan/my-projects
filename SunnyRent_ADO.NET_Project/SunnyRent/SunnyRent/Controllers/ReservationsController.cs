﻿using SunnyRent.Models;
using SunnyRent.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;


namespace SunnyRent.Controllers
{
    [Authorize]
    public class ReservationsController : Controller
    {
        private ApplicationDbContext _context;

        public ReservationsController()
        {
            _context = new ApplicationDbContext();
        }


        // GET: Reservations
        public ActionResult Book(int id)
        {
            var viewModel = new ReservationFormViewModel
            {
                House = _context.Houses.SingleOrDefault(h => h.Id == id)
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Book(ReservationFormViewModel reservationFormViewModel)
        {
            //Check if the form is valid
            //if (!ModelState.IsValid)
            //{
            //    //Return the same form back to the user
            //    var viewModel = new ReservationFormViewModel
            //    {
            //        House = _context.Houses.SingleOrDefault(h => h.Id == reservationFormViewModel.House.Id)
            //    };
            //    return View(viewModel);
            //}

            var userName = User.Identity.GetUserName();
            //dbContext.YourTable.Where(x => x.UserId = userId);
            var record = new Reservation()
            {
                CustomerName = userName,                
                House = _context.Houses.Single(h => h.Id == reservationFormViewModel.House.Id),
                CheckIn = reservationFormViewModel.Reservation.CheckIn,
                CheckOut = reservationFormViewModel.Reservation.CheckOut,
                NumberOfGuests = reservationFormViewModel.Reservation.NumberOfGuests
            };

            _context.Reservations.Add(record);
            _context.SaveChanges();


            return RedirectToAction("Cottages", "Houses");


        }
    }
}