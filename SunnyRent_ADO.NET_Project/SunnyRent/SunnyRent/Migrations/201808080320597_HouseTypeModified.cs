namespace SunnyRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HouseTypeModified : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Houses", "HouseType_Id", "dbo.HouseTypes");
            DropIndex("dbo.Houses", new[] { "HouseType_Id" });
            DropPrimaryKey("dbo.HouseTypes");
            AlterColumn("dbo.Houses", "HouseType_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.HouseTypes", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.HouseTypes", "Id");
            CreateIndex("dbo.Houses", "HouseType_Id");
            AddForeignKey("dbo.Houses", "HouseType_Id", "dbo.HouseTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Houses", "HouseType_Id", "dbo.HouseTypes");
            DropIndex("dbo.Houses", new[] { "HouseType_Id" });
            DropPrimaryKey("dbo.HouseTypes");
            AlterColumn("dbo.HouseTypes", "Id", c => c.Byte(nullable: false));
            AlterColumn("dbo.Houses", "HouseType_Id", c => c.Byte(nullable: false));
            AddPrimaryKey("dbo.HouseTypes", "Id");
            CreateIndex("dbo.Houses", "HouseType_Id");
            AddForeignKey("dbo.Houses", "HouseType_Id", "dbo.HouseTypes", "Id", cascadeDelete: true);
        }
    }
}
