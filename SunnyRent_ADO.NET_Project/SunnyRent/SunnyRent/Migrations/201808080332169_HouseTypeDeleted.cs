namespace SunnyRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HouseTypeDeleted : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Houses", "HouseType_Id", "dbo.HouseTypes");
            DropIndex("dbo.Houses", new[] { "HouseType_Id" });
            AddColumn("dbo.Houses", "HouseType", c => c.String(nullable: false));
            DropColumn("dbo.Houses", "HouseType_Id");
            DropTable("dbo.HouseTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.HouseTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Houses", "HouseType_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Houses", "HouseType");
            CreateIndex("dbo.Houses", "HouseType_Id");
            AddForeignKey("dbo.Houses", "HouseType_Id", "dbo.HouseTypes", "Id", cascadeDelete: true);
        }
    }
}
