namespace SunnyRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HouseTypeModifiedInHouse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Houses", "HouseType_Id", c => c.Byte(nullable: false));
            CreateIndex("dbo.Houses", "HouseType_Id");
            AddForeignKey("dbo.Houses", "HouseType_Id", "dbo.HouseTypes", "Id", cascadeDelete: true);
            DropColumn("dbo.Houses", "HouseType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Houses", "HouseType", c => c.String(nullable: false));
            DropForeignKey("dbo.Houses", "HouseType_Id", "dbo.HouseTypes");
            DropIndex("dbo.Houses", new[] { "HouseType_Id" });
            DropColumn("dbo.Houses", "HouseType_Id");
        }
    }
}
