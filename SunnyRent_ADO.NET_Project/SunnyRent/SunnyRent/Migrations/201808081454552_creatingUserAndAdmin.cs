namespace SunnyRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creatingUserAndAdmin : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a0c2f979-8ac0-420e-b97a-ddf5c6d6574d', N'Customer@sunnyrent.com', 0, N'AP3mM9LIY1br7TrvAqFlm8bioiyGuDbg2LiWkIO/sa31L3kumMimeU5YIBBirN4Oxw==', N'fcde9f9b-56fc-42d6-bebb-187b55b06797', NULL, 0, 0, NULL, 1, 0, N'Customer@sunnyrent.com')
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ac7aa9be-aa6c-4aa0-85d1-5810114f77e9', N'Admin@admin.com', 0, N'AE+LrEwoRgjsotBAWFcckWU/vcOgb8U+vYxUV9fs/fKrhV1yYYcMKjMuMXUhJhBiqw==', N'fd44f560-decf-437d-b726-6533c7058339', NULL, 0, 0, NULL, 1, 0, N'Admin@admin.com')

            INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'59a14865-a406-4047-888f-8496c4ac17bc', N'CanManageMedia')

            INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ac7aa9be-aa6c-4aa0-85d1-5810114f77e9', N'59a14865-a406-4047-888f-8496c4ac17bc')

            ");
        }
        
        public override void Down()
        {
        }
    }
}
