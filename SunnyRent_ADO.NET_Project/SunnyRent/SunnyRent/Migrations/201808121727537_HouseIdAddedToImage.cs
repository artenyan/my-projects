namespace SunnyRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HouseIdAddedToImage : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Images", "House_Id", "dbo.Houses");
            DropIndex("dbo.Images", new[] { "House_Id" });
            RenameColumn(table: "dbo.Images", name: "House_Id", newName: "HouseId");
            AlterColumn("dbo.Images", "HouseId", c => c.Int(nullable: false));
            CreateIndex("dbo.Images", "HouseId");
            AddForeignKey("dbo.Images", "HouseId", "dbo.Houses", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "HouseId", "dbo.Houses");
            DropIndex("dbo.Images", new[] { "HouseId" });
            AlterColumn("dbo.Images", "HouseId", c => c.Int());
            RenameColumn(table: "dbo.Images", name: "HouseId", newName: "House_Id");
            CreateIndex("dbo.Images", "House_Id");
            AddForeignKey("dbo.Images", "House_Id", "dbo.Houses", "Id");
        }
    }
}
