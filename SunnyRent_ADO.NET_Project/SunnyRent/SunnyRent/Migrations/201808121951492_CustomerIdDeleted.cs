namespace SunnyRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerIdDeleted : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reservations", "CustomerId", "dbo.Customers");
            DropIndex("dbo.Reservations", new[] { "CustomerId" });
            DropColumn("dbo.Reservations", "CustomerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reservations", "CustomerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reservations", "CustomerId");
            AddForeignKey("dbo.Reservations", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
        }
    }
}
