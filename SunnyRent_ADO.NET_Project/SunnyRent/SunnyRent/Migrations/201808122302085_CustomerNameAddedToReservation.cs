namespace SunnyRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerNameAddedToReservation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "CustomerName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reservations", "CustomerName");
        }
    }
}
