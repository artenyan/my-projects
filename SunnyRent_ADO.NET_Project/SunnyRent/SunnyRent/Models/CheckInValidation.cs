﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SunnyRent.Models
{
    public class CheckInValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var reservation = (Reservation)validationContext.ObjectInstance;
            var dateCheckIn = reservation.CheckIn.Date;
            var dateCheckOut = reservation.CheckOut.Date;

            //If customer chooses the date after today or today--> no validation error
            if (reservation.CheckIn.CompareTo(DateTime.Now) < -1)
            {
                return new ValidationResult("Check In date is not valid");
            }
            //1-CheckOut not entered --> Validation error
            else if (reservation.CheckOut == null)
            {
                return new ValidationResult("Check Out date is required for Reservation");
            }
            //2-Date is enter
            else if (dateCheckOut.CompareTo(dateCheckIn) <= 0)
            {
                //2.1 Bellow dateCheckOut --> error
                return new ValidationResult("Check Out date can not be less or the same as checkIn  date");
            }
            else
            {
                //2.2 dateCheckIn above dateCheckOut --> no error
                return ValidationResult.Success;
            }
        }
    }
}