﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SunnyRent.Models
{
    public class House
    {     
        public int Id { get; set; }

        [Required(ErrorMessage ="House type field is required.")]
        [Display(Name = "Property Type")]
        public HouseType HouseType { get; set; }
       // public byte HouseTypeId { get; set; }

        [Required(ErrorMessage = "Rate field is required.")]
        public double Rate { get; set; }

        [Required(ErrorMessage = "Name field is required.")]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<Image> Image { get; set; }

        [Required(ErrorMessage = "Address field is required.")]
        public string Address { get; set; }

        
        public bool TV { get; set; }

        
        public bool Pet { get; set; }

        
        public bool BBQ { get; set; }

       
        public bool FirePlace { get; set; }

       
        public bool AccessToWater { get; set; }

        
        public bool Pool { get; set; }

       
        public bool Smoking { get; set; }

       
        public bool WiFi { get; set; }
    }
}
