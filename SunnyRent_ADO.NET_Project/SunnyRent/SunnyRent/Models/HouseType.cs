﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunnyRent.Models
{
    public class HouseType
    {
        public byte Id { get; set; }
        public string Type { get; set; }
    }
}