﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SunnyRent.Models
{
    public class Image
    {
        public int Id { get; set; }

        [Required]
        public string ImageName { get; set; }

        [Required]
        public string ImagePath { get; set; }

        public int HouseId { get; set; }
    }
}