﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SunnyRent.Models
{
    public class Reservation

    {
        public int Id { get; set; }
        
        public House House { get; set; }
        public int HouseId { get; set; }

        public string CustomerName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CheckIn { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CheckOut { get; set; }

        [Display(Name ="Number of Guests")]
        public byte NumberOfGuests { get; set; }

        
    }
}