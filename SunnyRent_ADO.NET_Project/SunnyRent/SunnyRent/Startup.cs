﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SunnyRent.Startup))]
namespace SunnyRent
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
