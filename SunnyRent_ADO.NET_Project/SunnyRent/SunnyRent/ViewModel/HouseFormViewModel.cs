﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SunnyRent.Models;

namespace SunnyRent.ViewModel
{
    public class HouseFormViewModel
    {
        public House House { get; set; }
        public IEnumerable<HouseType> HouseTypes { get; set; }
    }
}