﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SunnyRent.Models;

namespace SunnyRent.ViewModel
{
    public class HouseReservationViewModel
    {
        public House House { get; set; }
        public IEnumerable<Reservation> Reservations { get; set; }
    }
}