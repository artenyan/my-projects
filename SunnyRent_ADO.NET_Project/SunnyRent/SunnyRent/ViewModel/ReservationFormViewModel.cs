﻿using SunnyRent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunnyRent.ViewModel
{
    public class ReservationFormViewModel
    {
        public Reservation Reservation { get; set; }        
        public House House { get; set; }
    }
}